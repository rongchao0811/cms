//商品组件
import products from "../common/product/preview";
import productEdit from "../common/product/edit";

// 轮播图
import carousel from "../common/carousel/preview";
import carouselEdit from "../common/carousel/edit";

// 楼层间隔
import floorSpacing from "../common/floorSpacing/preview";
import floorSpacingEdit from "../common/floorSpacing/edit";
// 宣传标语
import slogan from "../common/slogan/preview";
import sloganEdit from "../common/slogan/edit";
// 分类（快捷入口）组件
import classify from "../common/classify/preview";
import classifyEdit from "../common/classify/edit";
// 图文广告
import advertisement from "../common/advertisement/preview";
import advertisementEdit from "../common/advertisement/edit";
// 品牌特卖
import brandSale from "../common/brandSale/preview";
import brandSaleEdit from "../common/brandSale/edit";
// 秒杀
import seckill from "../common/seckill/preview";
import seckillEdit from "../common/seckill/edit";
// 文字标题
import decorateTitle from "../common/decorate-title/preview";
import decorateTitleEdit from "../common/decorate-title/edit";
// 图片标题
import imgTitle from "../common/img-title/preview";
import imgTitleEdit from "../common/img-title/edit";
// 滚动广告
import slidesScroll from "../common/slides-scroll/preview";
import slidesScrollEdit from "../common/slides-scroll/edit";

// 底部提示
import bottomTips from "../common/bottom-tips/preview";
import bottomTipsEdit from "../common/bottom-tips/edit";
//公告
import notice from "../common/notice/preview";
import noticeEdit from "../common/notice/edit";
//一拖二
import oneToTwo from "../common/twoColumnPicAd/preview";
import oneToTwoEdit from "../common/twoColumnPicAd/edit";
//优惠劵
import coupon from "../common/coupon/preview";
import couponEdit from "../common/coupon/edit";
//推广广告
import promotion from "../common/promotion/preview";
import promotionEdit from "../common/promotion/edit";
//搜索
import search from "../common/search/preview";
import searchEdit from "../common/search/edit";
//吸顶
// import stickyLabel from "../common/stickyLabel/preview";
// import stickyLabelEdit from "../common/stickyLabel/edit";
//返回顶部
// import top from "../common/top/preview";
// import topEdit from "../common/top/edit";
//分类商品
// import classifyGoods from "../common/classifyGoods/preview";
// import classifyGoodsEdit from "../common/classifyGoods/edit";
// //测试tab
import tab from "../common/tab/preview";
import tabEdit from "../common/tab/edit";
//健康资讯
import pictureNews from "../common/pictureNews/preview";
import pictureNewsEdit from "../common/pictureNews/edit";
//京选推荐
import doctorNews from "../common/doctor/preview";
import doctorNewsEdit from "../common/doctor/edit";
//测试组件嵌套组件
import tabIcon from "../common/tabIcon/preview";
import tabIconEdit from "../common/tabIcon/edit";
//图片热区
//import hotArea from "../common/hotArea/preview";
//import hotAreaEdit from "../common/hotArea/edit";
//医生滚动
import doctorScroll from "../common/doctorScroll/preview";
import doctorScrollEdit from "../common/doctorScroll/edit";

//我的
import myInfo from "../common/myInfo/preview";
import myInfoEdit from "../common/myInfo/edit";


// 互联网医院
// // 文本说明
// import textLine from "../common/textLine/preview";
// import textLineEdit from "../common/textLine/edit";
// 疾病百科入口
import quickAccess from "../common/quickAccess/preview";
import quickAccessEdit from "../common/quickAccess/edit";
// banner入口
import bannerAccess from "../common/bannerAccess/preview";
import bannerAccessEdit from "../common/bannerAccess/edit";
// 医生团队
import doctorTeam from "../common/doctorTeam/preview";
import doctorTeamEdit from "../common/doctorTeam/edit";



const floorList = {};

//左侧组件标签 基本数据
const tags = [{
    category: "基础组件",
    list: [{
        name: "product",
        icon: "",
        type: "商品",
        tips: "商品组件",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            margin: ['0', '0', '0', '0'],
            padding: ["0", "0", "0", "0"],
            column: "1",
            cornerRadius: ["0", "0", "0", "0"]
          },
          ext: {
            type: "product",
            dataIds: {
              // groupId: "10808146", //商品组Id
            },
            dataSourceType1: "101",
            dataSourceType2: "101102",
            style: {
              margin: [0, 0, 0, 0],
              bgColor: "#ffffff"
            }
          },
          items: [{
            type: "product",
            style: {
              margin: [0, 0, 0, 0],
              bgColor: "#ffffff",
            },
            id: "",
            name: "",
            desc: "",
            image: null,
            pictureUrl: "",
            sparePictureUrl: "",
            hostPictureUrl: "",
            subData: {
              price: "",
              jdPrice: "",
            },
            hot: false,
            venderId: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "", //跳转页面的URL
            }
          }]
        }
      },
      {
        name: "carousel",
        icon: "",
        type: "轮播组件",
        tips: "轮播组件",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            margin: ['0', '0', '0', '0'],
            padding: ["0", "0", "0", "0"],
            column: "1",
            cornerRadius: ["0", "0", "0", "0"]
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "301",
            dataSourceType2: "301301",
            type: "banner",
          },
          items: [{
            type: "banner",
            style: {
              indicatorStyle: "none", //指示器样式
              indicatorRadius: 3, //指示器大小
              align: "center", //指示器位置
              infinite: true, //是否无限循环
              autoScroll: true,
              pageMargin: ["0", "0", "0", "0"],
              defaultIndicatorColor: "#333333",
              indicatorColor: "#B4B4B4", //选中的指示器的颜色
              pageHeight: 170, //轮播图高度
              hasIndicator: 1, //是否包含指示器 true
              margin: ["0", "0", "0", "0"],
              reuseId: "123",
              cornerRadius: ["0", "0", "0", "0"]
            },
            items: [{
              pictureUrl: "", //图片链接
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              }
            }, {
              pictureUrl: "",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              }
            }]
          }]
        }
      },
      {
        name: "floorSpacing",
        icon: "",
        type: "楼层间距",
        tips: "楼层间距",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#eeeeee",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
          },
          items: [{
            type: "spacing",
            style: {
              bgColor: "",
              height: 10,
              margin: ["0", "0", "0", "0"],
            }
          }]
        }
      },
      {
        name: "slogan",
        icon: "",
        type: "宣传标语",
        tips: "宣传标语",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "4",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            fontColor: "#333333", //字体颜色
            height: 35, //
            align: 'center', //
            sloganType: "0", //图片标题（1）还是文字标题（0）
            cornerRadius: ["0", "0", "0", "0"]
          },
          items: [{
              type: "text",
              text: "宣传标语",
              prefixSpace: "6", //圆点和文本间距
              prefixDoteRadius: "2", //圆点半径
              textColor: "#333333",
              fontSize: "10",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                bgColor: "",
                height: 35,
                margin: ["0", "0", "0", "0"],
              }
            },
            {
              type: "text",
              text: "宣传标语",
              prefixSpace: "6", //圆点和文本间距
              prefixDoteRadius: "2", //圆点半径
              textColor: "#333333",
              fontSize: "10",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                bgColor: "",
                height: 35,
                margin: ["0", "0", "0", "0"],
              }
            },
            {
              type: "text",
              text: "宣传标语",
              prefixSpace: "6", //圆点和文本间距
              prefixDoteRadius: "2", //圆点半径
              textColor: "#333333",
              fontSize: "10",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                bgColor: "",
                height: 35,
                margin: ["0", "0", "0", "0"],
              }
            },
            {
              type: "text",
              text: "宣传标语",
              prefixSpace: "6", //圆点和文本间距
              prefixDoteRadius: "2", //圆点半径
              textColor: "#333333",
              fontSize: "10",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                bgColor: "",
                height: 35,
                margin: ["0", "0", "0", "0"],
              }
            }
          ]
        }
      },
      {
        name: "classify",
        icon: "",
        type: "分类入口",
        tips: "分类入口",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            row: "2", //
            column: "5",
            cornerRadius: ["0", "0", "0", "0"],
            itemCornerRadiusValue: 0, //装图片的盒子圆角
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "10", "0"],
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "301",
            dataSourceType2: "301301",
            type: "enreyCube",
            style: {
              height: 60,
              display: "inline",
              fontSize: "12",
              titleColor: "333333",
              marginTop: "8",
              subTitleShow: false,
              subFontSize: "12",
              subTitleColor: "#aaaaaa",
              colspan: "",
              reuseId: "",
              picWidth: 40,
              picHeight: 40,
              margin: ["0", "0", "0", "0"],
              itemCornerRadius: ["0", "0", "0", "0"]
            }
          },
          items: [{
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, {
            type: "enreyCube",
            name: "",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            desc: ""
          }, ]

        }
      },
      {
        name: "advertisement",
        icon: "",
        type: "图文广告",
        tips: "图文广告",
        id: null,
        data: {
          type: "40",
          style: {
            adStyle: "0", //图文广告类型（布局类型和固定样式）
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            itemCornerRadius: 0, //装图片盒子的圆角
            height: 100, //
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            cornerRadius: ["0", "0", "0", "0"],
            columnSpace: 5,
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "301",
            dataSourceType2: "301301",
            type: "image",
          },
          items: [{
            type: "image",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            style: {
              bgColor: "#ffffff",
              height: 100,
              margin: ["0", "5", "0", "0"],
              cornerRadius: ["0", "0", "0", "0"],
              columnSpace: 5,
            }
          }]
        }
      },
      {
        name: "brandSale",
        icon: "",
        type: "品牌特卖",
        tips: "品牌特卖",
        id: null,
        data: {
          type: "40",
          style: {
            height: 120,
            margin: ["0", "0", "0", "0"],
            padding: ["0", "10", "0", "10"],
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "2",
            cornerRadius: ["0", "0", "0", "0"]
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "301",
            dataSourceType2: "301301",
            type: "limit_time_product",
          },
          items: [{
              type: "limit_time_product",
              descriptionText: "仅剩",
              date: "",
              countSec: "87000",
              hour: "08",
              minu: "08",
              sec: "08",
              pictureUrl: "",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                height: 120,
                colspan: "1",
                reuseId: "",
                margin: ["0", "2", "0", "2"]
              },
            },
            {
              type: "limit_time_product",
              descriptionText: "仅剩",
              date: "",
              countSec: "87000",
              hour: "08",
              minu: "08",
              sec: "08",
              pictureUrl: "",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                height: 120,
                colspan: "1",
                reuseId: "",
                margin: ["0", "2", "0", "2"]
              },
            }
          ]
        }
      },
      {
        name: "seckill",
        icon: "",
        type: "秒杀",
        tips: "秒杀",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["10", "10", "10", "10"],
            cornerRadius: ["0", "0", "0", "0"]
          },
          items: [{
            type: "limit_time_title",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            headImgUrl: '',
            tailImgUrl: '',
            counterDescriptionText: '12点场',
            tailText: "更多秒杀",
            countSec: "8700",
            hour: "10",
            minu: "30",
            sec: "40",
            style: {
              height: 30,
              linkColor: "#ff0000",
              reuseId: "",
              margin: ["0", "0", "0", "0"]
            },
          }]
        }
      },
      {
        name: "decorateTitle",
        icon: "",
        type: "文字标题",
        tips: "文字标题",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "10", "0", "10"],
            cornerRadius: ["0", "0", "0", "0"]
          },
          items: [{
            type: "titleRow",
            id: "title1",
            title: "楼层主标题",
            subTitle: "楼层副标题",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            fontSize: "15",
            align: "left",
            titleColor: "#333333",
            fontWeight: "normal",
            subTitleShow: true,
            subFontSize: "13",
            subTitleColor: "#aaaaaa",
            linkShow: false,
            linkStyle: "2",
            linkColor: "#333333",
            style: {
              height: 50,
              display: "inline",
              colspan: "",
              reuseId: "",
              margin: ["0", "0", "0", "0"],
            }
          }]
        }
      },
      {
        name: "imgTitle",
        icon: "",
        type: "图片标题",
        tips: "图片标题",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#eeeeee",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            cornerRadius: ["0", "0", "0", "0"]
          },
          items: [{
            type: "image",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            style: {
              bgColor: "#45b97c",
              height: 45,
              margin: ["0", "0", "0", "0"],
            }
          }]
        }
      },
      {
        name: "slidesScroll",
        icon: "",
        type: "横向滚动",
        tips: "横向滚动",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            cornerRadius: ["0", "0", "0", "0"]
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "301",
            dataSourceType2: "301301",
            type: "zoom_banner",
            style: {
              bgColor: "#FFFFFF",
              indicatorRadius: "3",
              infinite: true,
              autoScroll: "3000",
              scaleFactor: 0.2,
              lineSpacing: 6,
              pageMargin: ["0", "0", "0", "0"],
              pageHeight: 200,
              animation: "desc",
              cornerRadius: 10,
              margin: ["0", "0", "0", "10"]
            },
          },
          items: [{
              type: "zoom_banner",
              items: [{
                  pictureUrl: "",
                  jumpLinkInfo: {
                    linkType: "0", //跳转类型
                    identityId: "", //跳转页面的链接
                    linkUrl: "",
                  },
                },
                {
                  pictureUrl: "",
                  jumpLinkInfo: {
                    linkType: "0", //跳转类型
                    identityId: "", //跳转页面的链接
                    linkUrl: "",
                  },
                }
              ],
            },

          ]
        }
      },
      // {
      //   name: "textLine",
      //   icon: "",
      //   type: "文本说明",
      //   tips: "文本说明",
      //   data: {
      //     type: "40",
      //     style: {
      //       bgColor: "#2d8cf0",
      //       bgImgUrl: "",
      //       column: "1",
      //       margin: ["0", "0", "0", "0"],
      //       padding: ["0", "0", "0", "0"],
      //       fontColor: "#333333", //字体颜色
      //       marginType: []
      //     },
      //     items: [{
      //       type: "text",
      //       text: "宣传标语",
      //       prefixSpace: "6", //圆点和文本间距
      //       prefixDoteRadius: "2", //圆点半径
      //       textColor: "#FFFFFF",
      //       doteColor: "#FFFFFF",
      //       style: {
      //         bgColor: "",
      //         height: 35,
      //         margin: ["0", "0", "0", "0"],
      //       }
      //     }]
      //   }
      // },
      {
        name: "bottomTips",
        icon: "",
        type: "底部提示",
        tips: "底部提示",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            cornerRadius: ["0", "0", "0", "0"]
          },
          items: [{
              type: "bottomTips",
              tipText: "点击查看更多专家医师",
              arrowImgUrl: "",
              pictureUrl: "",
              jumpLinkInfo: {
                linkType: "0", //跳转类型
                identityId: "", //跳转页面的链接
                linkUrl: "",
              },
              style: {
                height: 50,
                fontColor: "#333333",
                display: "",
                colspan: "",
                reuseId: "",
                margin: ["0", "0", "0", "0"],
              }
            },

          ]
        }

      },
      {
        name: "oneToTwo",
        icon: "",
        type: "一拖二",
        tips: "一拖二",
        division: "0",
        maskColor: "",
        title: '0',
        titleIcon: '0',
        id: null,
        data: {
          type: "5",
          style: {
            bgColor: "#ffffff",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "10", "0", "10"],
          },
          items: []

        }
      },
      {
        name: "search",
        icon: "",
        type: "搜索",
        tips: "搜索",
        location: "0",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#5ac2b300",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            column: "1",
            prefixDoteRadius: ['6', '6', '6', '6'],
            bgImgUrl: ""
          },
          items: [{
            type: "search",
            action: "",
            searchImgUrl: "https://m.360buyimg.com/pop/jfs/t1/101693/30/1467/944/5dbf9d55E61af1ce1/3c4d84e8f2b5b6cb.png",
            searchText: "",
            style: {
              height: "42",
              display: "inline",
              bgColor: "#ffffff33",
              colspan: "1",
              reuseId: "",
              align: "left",
              margin: ["0", "0", "0", "0"]
            }
          }],
        }
      },
      {
        name: "tab",
        icon: "",
        type: "多tab导航",
        tips: "多tab导航",
        tabSelectedType: "0", //默认选中态border
        secondShow: "0", //不展示二级标签
        id: null,
        data: [{
            type: "stickyOff", //stickyOff表示吸顶 40表示不吸顶
            style: {
              freeOffset: "", //楼层吸顶管理的距离
              bgColor: "#ffffff",
              bgImgUrl: "",
              padding: ["0", "0", "0", "10"],
              margin: ["0", "10", "0", "10"],
              column: "5", //后期标题是定宽不定列
              prefixDoteRadius: ['0', '0', '6', '6']
            },
            ext: {
              dataSourceId: "", //groupId
              dataSourceType1: "201", //数据来源 ace 写手后台
              dataSourceType2: "", //数据来源下的数据类型 广告 商品
            },
            items: [{
              type: "scrollMenu",
              nextLayoutId: "", //点击选项卡会影响到的楼层
              style: {
                reuseId: "scrollMenu - 2",
                bgColor: "#ffffff",
                titleSizeNormal: "16",
                titleSizeSelected: "18",
                titleColorNormal: "#999999",
                titleColorSelected: "#333333",
                selectedIndex: 0,
              },
              titles: [{}, {}]
            }]
          },
          {
            type: "40", //stickyOff表示吸顶 40表示不吸顶
            style: {
              bgColor: "#ffffff",
              bgImgUrl: "",
              padding: ["0", "0", "0", "10"],
              margin: ["0", "10", "0", "10"],
              column: "5", //后期标题是定宽不定列
              prefixDoteRadius: ['0', '0', '6', '6']
            },
          }
        ],
        insert: []
      },
      {
        name: "myInfo",
        icon: "",
        type: "我的",
        tips: "我的",
        id: null,
        data: {
          type: "40",
          style: {
            bgColor: "#eb3c3c",
            column: "1"
          },
          items: [{
            type: "IdinforHead",
            headImgUrl: "https://m.360buyimg.com/pop/jfs/t1/58672/33/15656/174565/5dca9be5Ef6fe051b/063e7a8c3eb9b926.png",
            name: "jd_58934",
            price: "小白信用82分",
            tags: [{
                count: "10",
                name: "商品关注"
              },
              {
                count: "10",
                name: "店铺关注",
                redPoint: "true"
              },
              {
                count: "10",
                name: "喜欢的内容"
              },
              {
                count: "10",
                name: "浏览数"
              }
            ],
            adContent: "plus会员 您有1300京豆待激活",
            adTips: "立即开通",
            style: {
              height: "230",
              display: "inline",
              fontColor: "#ffffff",
              reuseId: "",
              margin: ["0", "0", "0", "0"]
            }
          }]
        },
      },
      {
        name: "doctorScroll",
        icon: "",
        type: "我的医生",
        tips: "我的医生",
        id: null,
        data: {
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "201",
            dataSourceType2: "201202",
          },
          tabStyle: {
            bgColor: "#ffffff",
            padding: ["0", "0", "0", "0"],
            margin: ["0", "0", "0", "0"],
          },
          style: {
            bgColor: '#ffffff',
            titleColor: '#2E2D2D',
            titleFontSize: '15',
            titleWeight: 'bold',
            labelFontSize: '11',
            labelColor: '#1F88F8',
            labelBgColor: '#e8f3fe',
            labelBorderRadius: ['0', '0', '0', '0'],
            avatarRadius: ['100', '100', '100', '100'],
            avatarFontSize: '11',
            avatarColor: '#9396a0',
            avatarBgColor: '#f4f6fb',
            pictureRadius: ['0', '16', '16', '0'],
            pictureLabelFontSize: '10',
            pictureLabelColor: '#ffffff',
            padding: ["10", "10", "10", "10"],
            margin: ["0", "0", "0", "0"],
            prefixDoteRadius: ['6', '6', '6', '6']
          },
        }
      },
      {
        name: "pictureNews",
        icon: "",
        type: "健康资讯",
        tips: "健康资讯",
        id: null,
        data: {
          style: {
            bgColor: '#ffffff',
            titleColor: '#2E2D2D',
            titleFontSize: '15',
            titleWeight: 'bold',
            labelFontSize: '11',
            labelColor: '#1F88F8',
            labelBgColor: '#e8f3fe',
            labelBorderRadius: ['0', '0', '0', '0'],
            avatarRadius: ['100', '100', '100', '100'],
            avatarFontSize: '11',
            avatarColor: '#9396a0',
            avatarBgColor: '#f4f6fb',
            pictureRadius: ['0', '16', '16', '0'],
            pictureLabelFontSize: '10',
            pictureLabelColor: '#ffffff'
          },
          items: []
        }
      },
      {
        name: "doctorNews",
        icon: "",
        type: "京选推荐",
        tips: "京选推荐",
        id: null,
        data: {
          style: {
            bgColor: '#ffffff',
            titleColor: '#2E2D2D',
            titleFontSize: '15',
            titleWeight: 'bold',
            labelFontSize: '11',
            labelColor: '#1F88F8',
            labelBgColor: '#e8f3fe',
            labelBorderRadius: ['0', '0', '0', '0'],
            avatarRadius: ['100', '100', '100', '100'],
            avatarFontSize: '11',
            avatarColor: '#9396a0',
            avatarBgColor: '#f4f6fb',
            pictureRadius: ['0', '16', '16', '0'],
            pictureLabelFontSize: '10',
            pictureLabelColor: '#ffffff',
            padding: ["10", "10", "10", "10"],
            margin: ["0", "0", "0", "0"],
            prefixDoteRadius: ['0', '0', '6', '6']
          },
        }
      },
      // {
      //     name: "tabIcon",
      //     icon: "",
      //     type: "吸顶",
      //     tips: "吸顶",
      //     tabType: "0", //默认文字式
      //     tabSelectedType: "0", //默认选中态border
      //     secondShow: "0", //不展示二级标签
      //     fixed: "0", //是否吸顶
      //     data: {
      //         type: "stickyOff", //不确定 应该是需要修改
      //         id: "",
      //         ext: {
      //             dataSourceId: "",
      //             dataSourceType1: "201",
      //             dataSourceType2: "201202",
      //         },
      //         tabStyle: {
      //             bgColor: "#ffffff",
      //             padding: ["0", "0", "0", "0"],
      //             margin: ["0", "0", "0", "0"],
      //         },
      //         contentStyle: {
      //             bgColor: "#ffffff",
      //             padding: ["0", "0", "0", "0"],
      //             margin: ["0", "0", "0", "0"],
      //         },
      //         tabData: [],
      //         style: {
      //             freeOffset: "", //不确定 应该不需要传递数据
      //             bgColor: "#ffffff",
      //             padding: ["10", "10", "10", "10"],
      //             margin: ["0", "0", "0", "0"],
      //             column: "5",
      //             prefixDoteRadius: ['0', '0', '6', '6']
      //         },
      //         tabDataList: [],
      //         data: [
      //             {
      //                 name: ''
      //             },
      //             {
      //                 name: ''
      //             }
      //         ],
      //         items: [{
      //             type: "scrollMenu", //不确定 应该需要修改
      //             nextLayoutId: "", //不确定 应该是需要修改
      //             style: {
      //                 reuseId: "scrollMenu - 2",
      //                 bgColor: "#ffffff",
      //                 titleSizeNormal: "16",
      //                 titleSizeSelected: "18",
      //                 titleColorNormal: "#666666",
      //                 titleColorSelected: "#ff0000",
      //                 selectedIndex: "0"
      //             }
      //         }]
      //     }
      // }
    ]
  },
  {
    category: "辅助组件",
    id: null,
    list: [{
      name: "notice",
      icon: "",
      type: "公告",
      tips: "公告",
      noticeStyle: "1",
      data: {
        type: "40",
        style: {
          bgColor: "#ffffff",
          padding: ["0", "0", "0", "0"],
          margin: ["0", "0", "0", "0"],
          column: "1",
        },
        items: [{
          type: "news_scrol",
          jumpLinkInfo: {
            linkType: "0", //跳转类型
            identityId: "", //跳转页面的链接
            linkUrl: "",
          },
          headImgUrl: "http://babel.m.jd.com/active/babelTower/images/finance_icon.png",
          tailText: "更多",
          items: [],
          style: {
            pageHeight: "40",
            display: "inline",
            colspan: "1",
            reuseId: "",
            margin: ["0", "10", "0", "10"]
          }
        }]
      }
    }]
  },
  {
    category: "运营组件",
    id: null,
    list: [{
      name: "coupon",
      icon: "",
      type: "优惠券",
      tips: "优惠券",
      division: "0",
      couponImgUrl: "http://storage.360buyimg.com/tower/babelnode/static/media/custom_coupon_r_2.d7e073e0.png",
      data: {
        type: "40",
        style: {
          bgColor: "#ffffff",
          bgImgUrl: "",
          margin: ["0", "1", "0", "0"],
          padding: ["0", "10", "0", "10"],
          column: "2",
        },
        items: []
      }
    }, {
      name: "promotion",
      icon: "",
      type: "推广广告",
      tips: "推广广告",
      division: "0",
      data: {
        type: "40",
        bgImg: {
          url: '',
          style: {}
        },
        logoImg: {
          url: '',
          style: {
            width: '',
            top: '',
            left: ''
          }
        },
        textImg: {
          url: '',
          style: {
            width: '',
            top: '',
            left: ''
          }
        },
        textSubImg: {
          url: '',
          style: {
            width: '',
            top: '',
            left: ''
          }
        },
        style: {
          bgColor: "#ffffff",
          margin: ["0", "1", "0", "0"],
          padding: ["0", "10", "0", "10"],
          column: "2",
        },
      }
    }, ]
  },
  {
    category: "互联网医院",
    id: null,
    list: [{
      name: "quickAccess",
      icon: "",
      type: "疾病百科入口",
      tips: "疾病百科入口",
      data: {
        type: "40",
        style: {
          bgColor: "#ffffff",
          bgImgUrl: "",
          row: "2", //
          column: "4",
          cornerRadius: ["0", "0", "0", "0"],
          itemCornerRadiusValue: 5, //装图片的盒子圆角
          margin: ["0", "0", "0", "0"],
          padding: ["0", "10", "10", "10"],
        },
        ext: {
          dataIds: {},
          pageNo: 1,
          pageSize: 10,
          dataSourceType1: "301",
          dataSourceType2: "301301",
          type: "quickAccess",
          style: {
            height: 35,
            display: "inline",
            fontSize: "12",
            fontWeight: "normal",
            bgColor: "#eeeeee",
            titleColor: "333333",
            marginTop: "8",
            colspan: "",
            reuseId: "",
            picWidth: 40,
            picHeight: 40,
            margin: ["10", "0", "0", "0"],
            itemCornerRadius: ["5", "5", "5", "5"]
          }
        },
        items: [{
            type: "quickAccess",
            name: "甲状腺炎",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "高血压",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "脱发",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png',

          },
          {
            type: "quickAccess",
            name: "痘痘",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "糖尿病",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "胃炎",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "湿疹",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          },
          {
            type: "quickAccess",
            name: "感冒",
            pictureUrl: "",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            hostImgUrl: '',
          }
        ]
      }
    }, {
      name: "bannerAccess",
      icon: "",
      type: "banner入口",
      tips: "banner入口",
      data: {
        type: "40",
        style: {
          bgColor: "#ffffff",
          bgImgUrl: "",
          column: "3",
          margin: ["0", "0", "0", "0"],
          padding: ["0", "0", "10", "0"],
          cornerRadius: ["0", "0", "0", "0"]
        },
        ext: {
          dataIds: {},
          pageNo: 1,
          pageSize: 10,
          dataSourceType1: "301",
          dataSourceType2: "301301",
          type: "bannerAccess",
          style: {
            height: 91,
            bgColor: "#eeeeee",
            display: "inline",
            fontSize: "16",
            fontWeight: "normal",
            titleColor: "333333",
            subFontSize: "12",
            subTitleColor: "#aaaaaa",
            picWidth: 50,
            picHeight: 50,
            marginTop: "8",
            margin: ["10", "0", "0", "0"],
          }
        },
        items: [{
            type: "bannerAccess",
            name: "急速问诊",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            pictureUrl: '//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png',
            desc: "全职医生急速答复"
          },
          {
            type: "bannerAccess",
            name: "找医生",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            pictureUrl: '//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png',
            desc: "368+三甲医院名医"

          },
          {
            type: "bannerAccess",
            name: "开药门诊",
            jumpLinkInfo: {
              linkType: "0", //跳转类型
              identityId: "", //跳转页面的链接
              linkUrl: "",
            },
            pictureUrl: "//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png",
            desc: "正品药极速达"
          }
        ]
      }
    }, {
      name: "doctorTeam",
      icon: "",
      type: "医生团队",
      tips: "医生团队",
      data: {
        type: "40",
        style: {
          bgColor: "#ffffff",
          bgImgUrl: "",
          column: "2",
          margin: ["0", "0", "0", "0"],
          padding: ["0", "0", "0", "0"],
          cornerRadius: ["0", "0", "0", "0"]
        },
        ext: {
          dataIds: {},
          pageNo: 1,
          pageSize: 10,
          dataSourceType1: "301",
          dataSourceType2: "301301",
          type: "doctorTeam"
        },
        items: [{
          type: "doctorTeam",
          pictureUrl: "",
          jumpLinkInfo: {
            linkType: "0", //跳转类型
            identityId: "", //跳转页面的链接
            linkUrl: "",
            spareLinkUrl: [],
          },
          style: {
            bgColor: "#45b97c",
            height: 106,
            margin: [
              "0",
              "0",
              "0",
              "0"
            ]
          }
        }]
      }
    }]
  }
];

//中间预览区的组件
const componentsPreview = {
  product: products,
  carousel: carousel,
  floorSpacing: floorSpacing,
  slogan: slogan,
  classify: classify,
  advertisement: advertisement,
  brandSale: brandSale,
  seckill: seckill,
  decorateTitle: decorateTitle,
  imgTitle: imgTitle,
  slidesScroll: slidesScroll,
  // textLine: textLine,
  bottomTips: bottomTips,
  notice: notice,
  oneToTwo: oneToTwo,
  coupon: coupon,
  search: search,
  promotion: promotion,
  //   stickyLabel: stickyLabel,
  // top: top,
  //   classifyGoods: classifyGoods,
  tab: tab,
  myInfo: myInfo,
  quickAccess: quickAccess,
  bannerAccess: bannerAccess,
  doctorTeam: doctorTeam,
  doctorScroll: doctorScroll,
  pictureNews: pictureNews,
  doctorNews: doctorNews,
  tabIcon: tabIcon,
  //hotArea: hotArea
};
//右侧编辑区的组件
const componentEdit = {
  product: productEdit,
  carousel: carouselEdit,
  floorSpacing: floorSpacingEdit,
  slogan: sloganEdit,
  classify: classifyEdit,
  advertisement: advertisementEdit,
  brandSale: brandSaleEdit,
  seckill: seckillEdit,
  decorateTitle: decorateTitleEdit,
  imgTitle: imgTitleEdit,
  slidesScroll: slidesScrollEdit,
  // textLine: textLineEdit,
  bottomTips: bottomTipsEdit,
  notice: noticeEdit,
  oneToTwo: oneToTwoEdit,
  coupon: couponEdit,
  search: searchEdit,
  promotion: promotionEdit,
  //   stickyLabel: stickyLabelEdit,
  // top: topEdit,
  //   classifyGoods: classifyGoodsEdit,
  tab: tabEdit,
  myInfo: myInfoEdit,
  quickAccess: quickAccessEdit,
  bannerAccess: bannerAccessEdit,
  doctorTeam: doctorTeamEdit,
  doctorScroll: doctorScrollEdit,
  pictureNews: pictureNewsEdit,
  doctorNews: doctorNewsEdit,
  tabIcon: tabIconEdit,
  //hotArea: hotAreaEdit
};

export {
  componentsPreview,
  componentEdit,
  tags
};
