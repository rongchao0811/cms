/**
 * 打开数据库
 */
// 本演示使用的数据库名称
var dbName = 'pageList';
// 版本
var version = 1;
// 数据库数据结果
var db;

// 打开数据库
var DBOpenRequest = window.indexedDB.open(dbName, version);

// 如果数据库打开失败
DBOpenRequest.onerror = function(event) {
  logError('数据库打开失败');
};

DBOpenRequest.onsuccess = function(event) {
  // 存储数据结果
  db = DBOpenRequest.result;

  // 显示数据
  show();
};

// 下面事情执行于：数据库首次创建版本，或者window.indexedDB.open传递的新版本（版本数值要比现在的高）
DBOpenRequest.onupgradeneeded = function(event) {
  var db = event.target.result;

  db.onerror = function(event) {
    logError('数据库打开失败');
  };

  // 创建一个数据库存储对象
  var objectStore = db.createObjectStore(dbName, {
    keyPath: 'id',
    autoIncrement: true
  });

  // 定义存储对象的数据项
  objectStore.createIndex('id', 'id', {
    unique: true
  });
};
export const add = (newItem,cb) =>{
  var transaction = db.transaction([dbName], "readwrite");
  // 打开已经存储的数据对象
  var objectStore = transaction.objectStore(dbName);
  // 添加到数据对象中
  var objectStoreRequest = objectStore.add(newItem);
  objectStoreRequest.onsuccess = function(event) {
    show();
    if(cb){
      cb()
    }
  };
}
export const edit= (id, data,cb)=> {
  // 编辑数据
  var transaction = db.transaction([dbName], "readwrite");
  // 打开已经存储的数据对象
  var objectStore = transaction.objectStore(dbName);
  // 获取存储的对应键的存储对象
  var objectStoreRequest = objectStore.get(id);
  // 获取成功后替换当前数据
  objectStoreRequest.onsuccess = function(event) {
    // 当前数据
    var myRecord = objectStoreRequest.result;
    if(cb){
      cb(myRecord)
    }
    // 遍历替换
    for (var key in data) {
      if (typeof myRecord[key] != 'undefined') {
        myRecord[key] = data[key];
      }
    }
    // 更新数据库存储数据
    objectStore.put(myRecord);
    objectStore.onsuccess = function () {
      console.log('更新数据成功')
    }
  };
}
export const del=(id,cb)=> {
  // 打开已经存储的数据对象
  var objectStore = db.transaction([dbName], "readwrite").objectStore(dbName);
  // 直接删除
  var objectStoreRequest = objectStore.delete(id);
  // 删除成功后
  objectStoreRequest.onsuccess = function() {
    if(cb){
      cb()
    }
    show();
  };
}
export const show = (cb)=> {
  // 打开对象存储，获得游标列表
  var objectStore = db.transaction(dbName).objectStore(dbName);
  var result=[]
  objectStore.openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    // 如果游标没有遍历完，继续下面的逻辑
    if (cursor && cursor!==null) {
      // 继续下一个游标项
      cursor.continue();
      result.push(cursor.value);
      // console.log('@@@'+JSON.stringify(data))
      // 如果全部遍历完毕
    } else {
      console.log('全部查找数据成功')
      if(cb){
        cb(result)
      }
    }
  }
}
