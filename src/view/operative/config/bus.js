const install = (Vue) =>{
  const Bus = new Vue({
    data:{
      list:[],//楼层数据
      current: {
        edit: null, //当前编辑元素编辑区模板数据
        index: null //当前编辑元素索引
      },//当前编辑元素
    },
    created () {
      this.$on('clearList', this.clearList)//清空楼层数据
      this.$on('setCurrentIndex', this.setCurrentIndex)//设置当前选择楼层索引
      this.$on('saveList', this.saveList)//保存楼层数据
    },
    // 在组件销毁前清除事件监听
    beforeDestroy () {
      this.$off('clearList', this.clearList)
      this.$off('setCurrentIndex', this.setCurrentIndex)
      this.$off('saveList', this.saveList)
    },
    methods:{
      emit(event,...args){
        this.$emit(event,...args)
      },
      on(event, callback){
        this.$on(event,callback)
      },
      off (event,callback) {
        this.$off(event,callback)
      },
      clearList: function () {
        let len = this.list.length;
        this.list.splice(0, len);
      },
      //设置当前组件索引
      setCurrentIndex: function (data) {
        this.current.index = data.index
      },
      saveList (key = 'floorList') {
        sessionStorage.setItem(key, JSON.stringify(this.list))
      },
      getList (key='floorList') {
        return JSON.parse(sessionStorage.getItem(key))
      }
    }
  })
  Vue.prototype.$bus = Bus
}

export default install
