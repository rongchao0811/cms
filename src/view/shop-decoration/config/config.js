//商品组件
import products from "../common/product/preview";
import productEdit from "../common/product/edit";

// 轮播图
import carousel from "../common/carousel/preview";
import carouselEdit from "../common/carousel/edit";

// 楼层间隔
// import floorSpacing from "../common/floorSpacing/preview";
// import floorSpacingEdit from "../common/floorSpacing/edit";
// 宣传标语
import slogan from "../common/slogan/preview";
import sloganEdit from "../common/slogan/edit";
// 分类（快捷入口）组件
import classify from "../common/classify/preview";
import classifyEdit from "../common/classify/edit";
// 图文广告
import advertisement from "../common/advertisement/preview";
import advertisementEdit from "../common/advertisement/edit";
// 品牌特卖
import brandSale from "../common/brandSale/preview";
import brandSaleEdit from "../common/brandSale/edit";
// 秒杀
import seckill from "../common/seckill/preview";
import seckillEdit from "../common/seckill/edit";
// 文字标题
import decorateTitle from "../common/decorate-title/preview";
import decorateTitleEdit from "../common/decorate-title/edit";
// 图片标题
// import imgTitle from "../common/img-title/preview";
// import imgTitleEdit from "../common/img-title/edit";
// 滚动广告
import slidesScroll from "../common/slides-scroll/preview";
import slidesScrollEdit from "../common/slides-scroll/edit";
// 滚动标签
import tabScroll from "../common/tab-scroll/preview";
import tabScrollEdit from "../common/tab-scroll/edit";
// 底部提示
import bottomTips from "../common/bottom-tips/preview";
import bottomTipsEdit from "../common/bottom-tips/edit";
//公告
import notice from "../common/notice/preview";
import noticeEdit from "../common/notice/edit";
//一拖二
import oneToTwo from "../common/twoColumnPicAd/preview";
import oneToTwoEdit from "../common/twoColumnPicAd/edit";
//优惠劵
import coupon from "../common/coupon/preview";
import couponEdit from "../common/coupon/edit";
//推广广告
import promotion from "../common/promotion/preview";
import promotionEdit from "../common/promotion/edit";
//搜索
import search from "../common/search/preview";
import searchEdit from "../common/search/edit";
//吸顶
// import stickyLabel from "../common/stickyLabel/preview";
// import stickyLabelEdit from "../common/stickyLabel/edit";
//返回顶部
// import top from "../common/top/preview";
// import topEdit from "../common/top/edit";
//分类商品
import classifyGoods from "../common/classifyGoods/preview";
import classifyGoodsEdit from "../common/classifyGoods/edit";
// //测试tab
import tab from "../common/tab/preview";
import tabEdit from "../common/tab/edit";
//健康资讯
import pictureNews from "../common/pictureNews/preview";
import pictureNewsEdit from "../common/pictureNews/edit";
//京选推荐
import doctorNews from "../common/doctor/preview";
import doctorNewsEdit from "../common/doctor/edit";
//重叠楼层
import overlap from "../common/overlap/preview";
import overlapEdit from "../common/overlap/edit";
//测试组件嵌套组件
import tabIcon from "../common/tabIcon/preview";
import tabIconEdit from "../common/tabIcon/edit";
//图片热区
//import hotArea from "../common/hotArea/preview";
//import hotAreaEdit from "../common/hotArea/edit";
//医生滚动
import doctorScroll from "../common/doctorScroll/preview";
import doctorScrollEdit from "../common/doctorScroll/edit";
//好礼领取
import couponGift from "../common/couponGift/preview";
import couponGiftEdit from "../common/couponGift/edit";
//我的
import myService from "../common/myService/preview";
import myServiceEdit from "../common/myService/edit";
import myInfo from "../common/myInfo/preview";
import myInfoEdit from "../common/myInfo/edit";
import myAvatar from "../common/myAvatar/preview";
import myAvatarEdit from "../common/myAvatar/edit";
import myItem from "../common/myItem/preview";
import myItemEdit from "../common/myItem/edit";
import myButton from "../common/myButton/preview";
import myButtonEdit from "../common/myButton/edit";
import orderState from "../common/orderState/preview";
import orderStateEdit from "../common/orderState/edit";
import logistics from "../common/logistics/preview";
import logisticsEdit from "../common/logistics/edit";
// 互联网医院
// // 文本说明
// import textLine from "../common/textLine/preview";
// import textLineEdit from "../common/textLine/edit";
// 疾病百科入口
import quickAccess from "../common/quickAccess/preview";
import quickAccessEdit from "../common/quickAccess/edit";
// banner入口
// import bannerAccess from "../common/bannerAccess/preview";
// import bannerAccessEdit from "../common/bannerAccess/edit";
// 医生团队
import doctorTeam from "../common/doctorTeam/preview";
import doctorTeamEdit from "../common/doctorTeam/edit";
//京东APP-我的
//import blank from "../common/blank/preview";
//import blankEdit from "../common/blank/edit";
//京东APP-完成页
import accountOrder from "../common/accountOrder/preview";
import accountOrderEdit from "../common/accountOrder/edit";
//京东App-健康大药房
import pharmacy from "../common/pharmacy/preview";
import pharmacyEdit from "../common/pharmacy/edit";
import pharmacyTitle from "../common/pharmacyTitle/preview";
import pharmacyTitleEdit from "../common/pharmacyTitle/edit";

import {
	toggleBold
} from "simplemde";

const floorList = {};

//左侧组件标签 基本数据
const tags = [{
	category: "基础组件",
	list: [{
		name: "product",
		icon: "",
		type: "商品",
		tips: "商品组件",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#00ffffff",
				bgImgUrl: "",
				margin: ['0', '12', '0', '12'],
				padding: ["0", "0", "0", "0"],
				column: "2",
				cornerRadius: ["0", "0", "0", "0"],
				borderSize: ["1", "0", "0", "0"],
				borderStyle: ["solid", "solid", "solid", "solid"],//dotted点状 double双线 dashed虚线
				borderColor: ["#EFF0F0", "#EFF0F0", "#EFF0F0", "#EFF0F0"]
			},
			ext: {
				type: "product",
				dataIds: {
					// groupId: "10808146", //商品组Id
				},
				dataSourceType1: "101",
				dataSourceType2: "",
				pageSize: "2",
				style: {
					bgColor: "#ffffff",
					bgImgUrl: "",
					titleFontSize: "14",
					titleColor: "#262626",
					titleHeight: "46",
					//titleLineHeight: "28",
					moneyFontSize: "12",
					moneyColor: "#FF552E",
					priceFontSize: "18",
					priceColor: "#FF552E",
					priceFontWeight: "bold",
					priceMargin: ["10", "0", "0", "0"],
					jdPriceFontSize: "11",
					jdPriceColor: "#B3B3B3",
					moneyJdPriceFontSize: "11",
					moneyJdColor: "#B3B3B3",
					shopColor: "#cc4a0b",
					width: "170",
					height: "256",
					productImgWidth: "170",
					productImgHeight: "170",
					productImgPadding: ["0", "0", "0", "0"],
					productImgBgColor: "#FFFFFFFF",
					cornerRadius: ["8", "8", "8", "8"],
					gutter: "11",
					shadowRadius: "10",
					shadowColor: "#33F9F9F9",
					margin: ["10", "0", "0", "0"],
					padding: ["10", "10", "10", "10"],
					feedFlow: "true",
					shopShow: "false",
					jdPriceShow: "true",
					carBgImgUrl: "https://m.360buyimg.com/babel/jfs/t1/50981/28/10064/1740/5d75af08E11017a46/197ea492e3d5d82c.png",
					carWidth: "18",
					carHeight: "18",
					titleMargin: ["5", "0", "0", "0"],
					priceShow: "true",
					priceDisplay: "block",//inline
					priceAlign: "center",//left
					jdPriceDisplay: "block",
					jdPriceAlign: "center",
					borderMultiple: "1",
					borderRepeat: "false",
					borderSize: ["0", "1", "0", "0"],
					borderStyle: ["solid", "solid", "solid", "solid"],//dotted点状 double双线 dashed虚线
					borderColor: ["#EFF0F0", "#EFF0F0", "#EFF0F0", "#EFF0F0"],
					titleShow: "true",
				}
			},
			items: [{
				type: "product",
				id: "",
				name: "",
				desc: "",
				image: null,
				pictureUrl: "",
				sparePictureUrl: "",
				hostPictureUrl: "",
				price: "",
				jdPrice: "",
				hot: false,
				venderId: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "", //跳转页面的URL
				}
			}, {
				type: "product",
				id: "",
				name: "",
				desc: "",
				image: null,
				pictureUrl: "",
				sparePictureUrl: "",
				hostPictureUrl: "",
				price: "",
				jdPrice: "",
				hot: false,
				venderId: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "", //跳转页面的URL
				}
			}]
		}
	},
	{
		name: "carousel",
		icon: "",
		type: "轮播组件",
		tips: "轮播组件",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "banner",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				margin: ['0', '0', '0', '0'],
				padding: ["0", "0", "0", "0"],
				column: "1",
				aspectRatio: 2.206,
				cornerRadius: ["0", "0", "0", "0"],
				indicatorStyle: "none", //指示器样式
				indicatorRadius: 3, //指示器大小
				align: "center", //指示器位置
				infinite: true, //是否无限循环
				autoScroll: true,
				defaultIndicatorColor: "#333333",
				indicatorColor: "#B4B4B4", //选中的指示器的颜色
				hasIndicator: 1, //是否包含指示器 true
				reuseId: "123",
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "image",
				style: {
					bgColor: "",
					itemHeight: 170, //轮播图高度
					margin: ["0", "0", "0", "0"],
				},
			},
			items: [{
				type: "image",
				pictureUrl: "", //图片链接
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
			}, {
				type: "image",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
			}]
		}
	},
	// {
	//     name: "floorSpacing",
	//     icon: "",
	//     type: "楼层间距",
	//     tips: "楼层间距",
	//     id: null,
	//     data: {
	//         type: "40",
	//         style: {
	//             bgColor: "#eeeeee",
	//             bgImgUrl: "",
	//             column: "1",
	//             margin: ["0", "0", "0", "0"],
	//             padding: ["0", "0", "0", "0"],
	//         },
	//         items: [{
	//             type: "spacing",
	//             style: {
	//                 bgColor: "",
	//                 height: 10,
	//                 margin: ["0", "0", "0", "0"],
	//             }
	//         }]
	//     }
	// },
	{
		name: "slogan",
		icon: "",
		type: "宣传标语",
		tips: "宣传标语",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "4",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				fontColor: "#333333", //字体颜色
				height: 35, //
				align: 'center', //
				sloganType: "0", //图片标题（1）还是文字标题（0）
				cornerRadius: ["0", "0", "0", "0"]
			},
			items: [{
				type: "text",
				text: "宣传标语",
				prefixSpace: "6", //圆点和文本间距
				prefixDoteRadius: "2", //圆点半径
				textColor: "#333333",
				fontSize: "10",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					bgColor: "",
					height: 35,
					margin: ["0", "0", "0", "0"],
				}
			},
			{
				type: "text",
				text: "宣传标语",
				prefixSpace: "6", //圆点和文本间距
				prefixDoteRadius: "2", //圆点半径
				textColor: "#333333",
				fontSize: "10",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					bgColor: "",
					height: 35,
					margin: ["0", "0", "0", "0"],
				}
			},
			{
				type: "text",
				text: "宣传标语",
				prefixSpace: "6", //圆点和文本间距
				prefixDoteRadius: "2", //圆点半径
				textColor: "#333333",
				fontSize: "10",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					bgColor: "",
					height: 35,
					margin: ["0", "0", "0", "0"],
				}
			},
			{
				type: "text",
				text: "宣传标语",
				prefixSpace: "6", //圆点和文本间距
				prefixDoteRadius: "2", //圆点半径
				textColor: "#333333",
				fontSize: "10",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					bgColor: "",
					height: 35,
					margin: ["0", "0", "0", "0"],
				}
			}
			]
		}
	},
	{
		name: "classify",
		icon: "",
		type: "分类入口",
		tips: "分类入口",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				row: "2", //
				column: "5",
				cornerRadius: ["0", "0", "0", "0"],
				itemCornerRadiusValue: 0, //装图片的盒子圆角
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "10", "0"],
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "enreyCube",
				style: {
					height: 60,
					display: "inline",
					fontSize: "12",
					titleColor: "#333333",
					marginTop: "8",
					subTitleShow: false,
					subFontSize: "12",
					subTitleColor: "#aaaaaa",
					colspan: "",
					reuseId: "",
					picWidth: 40,
					picHeight: 40,
					margin: ["10", "0", "0", "0"],
					itemCornerRadius: ["0", "0", "0", "0"]
				}
			},
			items: [{
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			}, {
				type: "enreyCube",
				name: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				desc: ""
			},]

		}
	},
	{
		name: "advertisement",
		icon: "",
		type: "图文广告",
		tips: "图文广告",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				itemCornerRadius: 0, //装图片盒子的圆角
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "image",
				style: {
					bgColor: "#ffffff",
					itemHeight: 100,
					aspectRatio: 3.75,
					margin: ["0", "0", "0", "0"],
					cornerRadius: ["0", "0", "0", "0"],
				}
			},
			items: [{
				type: "image",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
			}]
		}
	},
	{
		name: "brandSale",
		icon: "",
		type: "品牌特卖",
		tips: "品牌特卖",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				height: 120,
				margin: ["0", "0", "0", "0"],
				padding: ["0", "10", "0", "10"],
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "2",
				cornerRadius: ["0", "0", "0", "0"]
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "limit_time_product",
			},
			items: [{
				type: "limit_time_product",
				descriptionText: "仅剩",
				date: "",
				countSec: "87000",
				hour: "08",
				minu: "08",
				sec: "08",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					height: 120,
					colspan: "1",
					reuseId: "",
					margin: ["0", "2", "0", "2"]
				},
			},
			{
				type: "limit_time_product",
				descriptionText: "仅剩",
				date: "",
				countSec: "87000",
				hour: "08",
				minu: "08",
				sec: "08",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					height: 120,
					colspan: "1",
					reuseId: "",
					margin: ["0", "2", "0", "2"]
				},
			}
			]
		}
	},
	{
		name: "seckill",
		icon: "",
		type: "秒杀",
		tips: "秒杀",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "0", "0", "0"],
				padding: ["10", "10", "10", "10"],
				cornerRadius: ["0", "0", "0", "0"]
			},
			items: [{
				type: "limit_time_title",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				headImgUrl: '',
				tailImgUrl: '',
				counterDescriptionText: '12点场',
				tailText: "更多秒杀",
				countSec: "8700",
				hour: "10",
				minu: "30",
				sec: "40",
				style: {
					height: 30,
					linkColor: "#ff0000",
					reuseId: "",
					margin: ["0", "0", "0", "0"]
				},
			}]
		}
	},
	{
		name: "decorateTitle",
		icon: "",
		type: "文字标题",
		tips: "文字标题",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "10", "0", "10"],
				cornerRadius: ["0", "0", "0", "0"]
			},
			items: [{
				type: "titleRow",
				id: "title1",
				title: "楼层主标题",
				subTitle: "楼层副标题",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				fontSize: "15",
				align: "left",
				titleColor: "#333333",
				fontWeight: "normal",
				subTitleShow: true,
				subFontSize: "13",
				subTitleColor: "#aaaaaa",
				linkShow: false,
				linkStyle: "2",
				linkColor: "#aaaaaa",
				style: {
					height: 50,
					display: "inline",
					colspan: "",
					reuseId: "",
					margin: ["0", "0", "0", "0"],
				}
			}]
		}
	},
	//   {
	//     name: "imgTitle",
	//     icon: "",
	//     type: "图片标题",
	//     tips: "图片标题",
	//     id: null,
	//     prodectId: "",
	//     pageId: "",
	//     pageIdentityId: "",
	//     data: {
	//       type: "40",
	//       style: {
	//         bgColor: "#eeeeee",
	//         bgImgUrl: "",
	//         column: "1",
	//         margin: ["0", "0", "0", "0"],
	//         padding: ["0", "0", "0", "0"],
	//         cornerRadius: ["0", "0", "0", "0"]
	//       },
	//       items: [{
	//         type: "image",
	//         pictureUrl: "",
	//         jumpLinkInfo: {
	//           linkType: "0", //跳转类型
	//           identityId: "", //跳转页面的链接
	//           linkUrl: "",
	//         },
	//         style: {
	//           bgColor: "#45b97c",
	//           height: 45,
	//           margin: ["0", "0", "0", "0"],
	//         }
	//       }]
	//     }
	//   },
	{
		name: "slidesScroll",
		icon: "",
		type: "横向滚动",
		tips: "横向滚动",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "zoom_banner",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"],
				indicatorRadius: "3",
				infinite: true,
				autoScroll: "3000",
				scaleFactor: 0.2,
				lineSpacing: 6,
				animation: "desc",
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "image",
				style: {
					bgColor: "#FFFFFF",
					itemHeight: 200,
					aspectRatio: 1.4833,
					margin: ["0", "0", "0", "10"]
				},
			},
			items: [{
				type: "image",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0",
					//跳转类型
					identityId: "",
					//跳转页面的链接
					linkUrl: "",
				},
			},
			{
				type: "image",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0",
					//跳转类型
					identityId: "",
					//跳转页面的链接
					linkUrl: "",
				},
			}
			],
		}
	},
      {
        name: "tabScroll",
        icon: "",
        type: "医生列表",
        tips: "医生列表",
        data: {
        floorBuryPoint: "",
          type: "stickyOff",
          style: {
            bgColor: "#ffffff",
            bgImgUrl: "",
            column: "1",
            margin: ["0", "0", "0", "0"],
            padding: ["0", "0", "0", "0"],
            cornerRadius: ["0", "0", "0", "0"],
          },
          ext: {
            dataIds: {},
            pageNo: 1,
            pageSize: 10,
            dataSourceType1: "201",
            dataSourceType2: "201202",
            type: "doctor",
            style: {
              bgColor: "#ffffff",
              avatarBgImg: "https://m.360buyimg.com/babel/jfs/t1/90819/20/8175/4923/5e022540E5e8f9af4/9a5b806c10b985a0.png",
              itemHeight: 100,
              margin: ["0", "0", "0", "0"],
              cornerRadius: ["0", "0", "0", "0"],
            }
          },
          items: [{
              type: "doctor",
              id:null,
              name:null,
              doctorResult: {
                doctorId: 167010051218,
                doctorName: "陈苗",
                doctorTitleName: "副主任医师",
                hospitalGradeName: "三甲医生",
                departName: "内科",
                doctorHeadPic: "https://img30.360buyimg.com/yiyaoapp/jfs/t1/97283/14/4514/54526/5de7a9f2E02a3e17c/baa413906b4a440e.jpg!q30",
                doctorGoodAt: "再生障碍性贫血、骨髓增生异常综合征、溶血性贫血、血色病、卟啉病及各种贫血、血细胞减少性疾病，白血病、淋巴瘤、多发性骨髓瘤。",
                famous: true,
                hospitalName: "北京协和医院",
                positiveRatio: 0.95,
                practitionersTime: null,
                fullRecommend: false,
                fastRx: true
              },

            },
            {
              type: "doctor",
              id:null,
              name:null,
              doctorResult: {
                doctorId: 202610336458,
                doctorName: "测试刘海滨",
                doctorTitleName: "副主任医师",
                hospitalGradeName: "三甲医生",
                departName: "内科",
                doctorHeadPic: "https://img30.360buyimg.com/yiyaoapp/jfs/t1/98689/5/1452/39181/5dbe8038Ee17bf740/8d7394486a229a69.jpg!q30",
                doctorGoodAt: "小儿脑瘫的早期诊断与康复，小儿癫痫，多动症，抽动症，发育迟缓",
                famous: false,
                hospitalName: "北京协和医院",
                positiveRatio: 1,
                practitionersTime: null,
                fullRecommend: false,
                fastRx: true,
              },
            },
            {
              type: "doctor",
              id:'33',
              name:'点击查看更多此科室',
              doctorResult: null,
              style: {
                bgColor: "#ffffff",
                itemHeight: 40,
                margin: ["0", "0", "0", "0"],
                cornerRadius: ["0", "0", "0", "0"],
              }
            }
          ]
        }
      },
	{
		name: "bottomTips",
		icon: "",
		type: "底部提示",
		tips: "底部提示",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"]
			},
			items: [{
				type: "bottomTips",
				tipText: "点击查看更多专家医师",
				arrowImgUrl: "",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				style: {
					height: 50,
					fontColor: "#333333",
					display: "",
					colspan: "",
					reuseId: "",
					margin: ["0", "0", "0", "0"],
				}
			},

			]
		}

	},
	{
		name: "oneToTwo",
		icon: "",
		type: "一拖二",
		tips: "一拖二",
		division: "0",
		maskColor: "",
		title: "0",
		titleIcon: "0",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "5",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "5",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "10", "0", "10"],
				cornerRadius: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {},
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "news",
			},
			items: [{
				type: "news",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				title: "",
				descriptionText: "",
				maskColor: "",
				maskImg: "",
				style: {
					height: "201",
					margin: ["0", "1", "0", "0"]
				}
			},
			{
				type: "news",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				title: "",
				descriptionText: "",
				maskColor: "",
				maskImg: "",
				style: {
					height: "100",
					margin: ["0", "0", "0", "0"]
				}
			},
			{
				type: "news",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				title: "",
				descriptionText: "",
				maskColor: "",
				maskImg: "",
				style: {
					height: "100",
					margin: ["0", "0", "0", "0"]
				}
			}
			]

		}
	},
	{
		name: "search",
		icon: "",
		type: "搜索",
		tips: "搜索",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#5AC2B3",
				bgImgUrl: "",
				margin: ["0", "0", "0", "0"],
				padding: ["10", "10", "10", "10"],
				column: "1",
				prefixDoteRadius: ["6", "6", "6", "6"],
				bgImgUrl: ""
			},
			items: [{
				type: "search",
				action: "",
				//互医 https://m.360buyimg.com/pop/jfs/t1/90648/16/11372/944/5e32b9a7Edf79e3b3/daae35a4211b64a4.png
				//首页 https://m.360buyimg.com/pop/jfs/t1/104224/29/8822/775/5e09bf34Ef3b5aa9e/c7d39e4cf03a9e8a.png
				searchImgUrl: "https://m.360buyimg.com/pop/jfs/t1/90648/16/11372/944/5e32b9a7Edf79e3b3/daae35a4211b64a4.png",
				searchText: "搜索",
				style: {
					height: "42",
					display: "inline",
					bgColor: "#33FFFFFF",
					location: "true",
					colspan: "1",
					reuseId: "",
					align: "left",
					margin: ["0", "0", "0", "0"],
					padding: ["0", "0", "0", "0"],
					cornerRadius: ["0", "0", "0", "0"]
				}
			}],
		}
	},
	{
		name: "tab",
		icon: "",
		type: "多tab导航",
		tips: "多tab导航",
		tabSelectedType: "0", //默认选中态border
		secondShow: "0", //不展示二级标签
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "stickyOff",
			feedFlow: "true",
			id: "",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "10", "0", "10"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["6", "6", "0", "0"],
				freeOffset: "", //楼层吸顶管理的距离
				prefixDoteRadius: ["6", "6", "0", "0"],
			},
			ext: {
				dataSourceId: "",
				dataSourceType1: "401",
				dataSourceType2: "",
				pageSize: "2",
				type: "scrollMenu"
			},
			items: [{
				type: "scrollMenu",
				action: [],
				nextLayoutId: "",
				style: {
					reuseId: "",
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					titleSizeNormal: "16",
					titleSizeSelected: "18",
					titleColorNormal: "#999999",
					titleColorSelected: "#FFFFFF",
					titleFontWeight: "bold",
					selectedIndex: 0,
					selectedBgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/108952/24/3058/181/5e0d7d9aE798bc7a2/dff97858c5bd47be.png",
					selectionBgUrl: "https://m.360buyimg.com/pop/jfs/t1/89492/28/9977/1047/5e1427ecEca97750b/2102d238c4495215.png", //标签背背景图带圆角 ios用
					defaultBgUrl: "https://m.360buyimg.com/pop/jfs/t1/101172/4/9774/602/5e142849Ebbe6c854/ec7eacd0fb5d591f.png", //标签背背景图带圆角 ios用
					margin: ["10", "0", "10", "0"],
					padding: ["0", "0", "0", "0"],
					titleMargin: ["0", "0", "0", "10"],
					titlePadding: ["0", "15", "0", "15"],
					cornerRadius: ["6", "6", "0", "0"],
					titleCornerRadius: ["20", "20", "20", "20"], //标签背景圆角
					titleSelectedImgUrl: "https://m.360buyimg.com/pop/jfs/t1/110290/30/3738/357/5e154d75E6fc37ca2/846369b7551c94c3.png", //标签背背景图不带圆角 安卓用
					titleDefaultImgUrl: "https://m.360buyimg.com/pop/jfs/t1/106224/28/10032/121/5e154d8aEdd11025a/b9e8418825a72212.png", //标签背背景图不带圆角 安卓用
					height: "36"
				},
				contentData: [{
					name: "标签",
					id: "",
					dataSourceType2: "", //101102商品
					pageSize: "10",
					dataIds: {
						id: "",
					}
				},
				{
					name: "标签",
					id: "",
					dataSourceType2: "", //101102商品
					pageSize: "10",
					dataIds: {
						id: "",
					}
				}
				],
			}],
		}
	},
	{
		name: "doctorScroll",
		icon: "",
		type: "我的医生",
		tips: "我的医生",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "201",
				dataSourceType2: "201202",
			},
			tabStyle: {
				bgColor: "#FFFFFF",
				padding: ["0", "0", "0", "0"],
				margin: ["0", "0", "0", "0"],
			},
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				margin: ["0", "0", "0", "0"],
				padding: ["10", "10", "10", "10"],
				cornerRadius: ["0", "0", "0", "0"],
				titleColor: "#2E2D2D",
				titleFontSize: "15",
				titleWeight: "bold",
				labelFontSize: "11",
				labelColor: "#1F88F8",
				labelBgColor: "#E8F3FE",
				labelBorderRadius: ["0", "0", "0", "0"],
				avatarRadius: ["100", "100", "100", "100"],
				avatarFontSize: "11",
				avatarColor: "#9396A0",
				avatarBgColor: "#F4F6FB",
				pictureRadius: ["0", "16", "16", "0"],
				pictureLabelFontSize: "10",
				pictureLabelColor: "#FFFFFF",
				prefixDoteRadius: ["6", "6", "6", "6"]
			},
		}
	},
	{
		name: "pictureNews",
		icon: "",
		type: "健康资讯",
		tips: "健康资讯",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "10", "10", "10"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "6", "6"],
				prefixDoteRadius: ["0", "0", "6", "6"],
			},
			ext: {
				dataSourceId: "",
				dataSourceType1: "401", //数据来源 ace 写手后台
				dataSourceType2: "", //数据来源下的数据类型 广告 商品
				type: "article",
				jumpLinkInfo: {
					linkType: "0",
					identityId: "",
					linkUrl: "",
					routerUrl: ""
				},
				style: {
					feedFlow: "true",
					bgColor: "#FFFFFF",
					margin: ["10", "0", "10", "0"],
					padding: ["0", "10", "0", "10"],
					cornerRadius: ["0", "0", "0", "0"],
					titleColor: "#2E2D2D",
					titleFontSize: "15",
					titleWeight: "bold",
					labelFontSize: "11",
					labelColor: "#1F88F8",
					labelBgColor: "#e8f3fe",
					labelBorderRadius: ["2", "2", "2", "2"],
					pictureRadius: ["8", "8", "8", "8"],
					pictureLabelRadius: ["40", "50", "50", "0"],
					pictureLabelColor: "#FFFFFF",
					pictureLabelFontSize: "10",
					pictureLabelBgUrl: "https://m.360buyimg.com/pop/jfs/t1/86678/21/10185/529/5e16e3c4Efb44315d/eb04aa2bc4b4bab1.png",
					pictureLabelPosition: "top_left",
					pictureLabelHeight: "60",
					pictureLabelWidth: "132",
					pictureWidth: "103",
					pictureHeight: "75",
					height: "75"
				},
			},
			items: [{
				id: "204705",
				name: "女人敷完面膜后，建议多做这几件事，补水效果会",
				desc: null,
				pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/97663/37/10549/37246/5e1c269fEde25503a/935d3ffc5b8affc3.jpg",
				sparePictureUrl: null,
				jumpLinkInfo: {
					linkType: null,
					linkUrl: "http://beta-h5.healthjd.com/article?id=204705",
					spareLinkUrl: null,
					routeLinkUrl: null,
					pageLinkUrl: null,
					identityId: null
				},
				hostPictureUrl: null,
				beginTime: null,
				endTime: null,
				price: null,
				jdPrice: null,
				labelInfos: [{
					id: "810",
					"name": "健康资讯",
					"sort": null
				}],
				assets: null,
				orderInfo: null,
				trackInfo: null,
				hot: false
			},
			{
				id: "204704",
				name: "眼霜不涂是罪，乱涂老十岁？",
				desc: null,
				pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/98996/3/10549/67477/5e1c2664Ea685140e/e5c0081fa6e5bbaa.jpg",
				sparePictureUrl: null,
				jumpLinkInfo: {
					linkType: null,
					linkUrl: "http://beta-h5.healthjd.com/article?id=204704",
					spareLinkUrl: null,
					routeLinkUrl: null,
					pageLinkUrl: null,
					identityId: null
				},
				hostPictureUrl: null,
				beginTime: null,
				endTime: null,
				price: null,
				jdPrice: null,
				labelInfos: [{
					id: "810",
					name: "健康资讯",
					sort: null
				}],
				assets: null,
				orderInfo: null,
				trackInfo: null,
				hot: false
			}
			]
		}
	},
	{
		name: "doctorNews",
		icon: "",
		type: "京选推荐",
		tips: "京选推荐",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				cornerRadius: ["0", "0", "0", "0"],
				titleColor: "#2E2D2D",
				titleFontSize: "15",
				titleWeight: "bold",
				labelFontSize: "11",
				labelColor: "#1F88F8",
				labelBgColor: "#E8F3FE",
				labelBorderRadius: ["0", "0", "0", "0"],
				avatarRadius: ["100", "100", "100", "100"],
				avatarFontSize: "11",
				avatarColor: "#9396A0",
				avatarBgColor: "#F4F6FB",
				pictureRadius: ["0", "16", "16", "0"],
				pictureLabelFontSize: "10",
				pictureLabelColor: "#FFFFFF",
				padding: ["10", "10", "10", "10"],
				margin: ["0", "0", "0", "0"],
				prefixDoteRadius: ["0", "0", "6", "6"]
			},
		}
	},
	{
		name: "overlap",
		icon: "",
		type: "轮播图重叠",
		tips: "轮播图重叠",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "bannerHead",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				column: "1",
				cornerRadius: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "image",
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					aspectRatio: 2.205,
					itemHeight: 200, //轮播图高度
					margin: ["0", "0", "0", "0"],
					padding: ["0", "0", "0", "0"],
					cornerRadius: ["0", "0", "0", "0"],
				},
			},
			bannerOther: {
				bannerSearch: {
					type: "bannerSearch",
					searchImgUrl: "https://m.360buyimg.com/pop/jfs/t1/102364/8/10628/2236/5e1dc05eEa10c2caa/6bf6f06330e7eec9.png",
					searchText: "搜医生/药品/疾病/体检"
				},
				bannerNav: {
					type: "bannerNav",
					positionShow: true, //是否显示位置信息
					titleImgUrl: "https://m.360buyimg.com/pop/jfs/t1/87539/34/10504/11452/5e1dbf17E28f3f6b4/9ec09b5b2b144fb9.png",
					shopInfo: {
						pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/93078/27/10460/1712/5e1dc019E7a9c78be/748b00edc1a0ba41.png",
						jumpLinkInfo: {
							linkType: "0", //跳转类型
							identityId: "", //跳转页面的链接
							linkUrl: "",
							routerUrl: ""
						},
					},
					messageInfo: {
						pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/108408/14/4397/1327/5e1dc034Ead4cabc7/1287d59aecf41964.png",
						jumpLinkInfo: {
							linkType: "0", //跳转类型
							identityId: "", //跳转页面的链接
							linkUrl: "",
							routerUrl: ""
						},
					},

				}
			},
			items: [{
				type: "image",
				pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/91603/25/10640/952286/5e1dbfdcE0bfc6bb0/fac6d1cd6399313a.png", //图片链接
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
			}, {
				type: "image",
				pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/106795/22/10725/744253/5e1e7e30E4407415d/bb3015c27b7f9f4e.png", //图片链接
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
			}]
		}
	}
	]
},
{
	category: "辅助组件",
	id: null,
	list: [{
		name: "notice",
		icon: "",
		type: "公告",
		tips: "公告",
		noticeStyle: "1",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "10", "0", "10"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"],
			},
			items: [{
				type: "news_scrol",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				headImgUrl: "http://babel.m.jd.com/active/babelTower/images/finance_icon.png",
				tailText: "更多",
				items: [{
					title: "",
					color: "#FF0000",
					content: "",
					imgUrl: "",
					message: "",
				}],
				style: {
					pageHeight: "40",
					display: "inline",
					colspan: "1",
					reuseId: "",
					margin: ["0", "10", "0", "10"]
				}
			}]
		}
	}]
},
{
	category: "运营组件",
	id: null,
	list: [{
		name: "coupon",
		icon: "",
		type: "优惠券",
		tips: "优惠券",
		couponImgUrl: "http://storage.360buyimg.com/tower/babelnode/static/media/custom_coupon_r_2.d7e073e0.png",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "2",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "10", "0", "10"],
				cornerRadius: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {},
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "coupon",
				style: {
					height: "80",
					tipsRatio: "0.20", //1图 177-140/177=0.20  2图 75/177=0.42 3图 0
					colspan: "",
					reuseId: "",
					margin: ["0", "1", "0", "0"],
					padding: ["0", "10", "0", "10"],
					moneyFontSize: "12",
					moneyFontColor: "#FFFFFF",
					moneyMargin: ["0", "3", "0", "0"],
					priceFontSize: "20",
					priceFontColor: "#FFFFFF",
					priceMargin: ["0", "0", "0", "0"],
					priceFontWeight: "bold",
					priceDisplay: "block",
					priceAlign: "center",
					discountFontSize: "12",
					discountFontColor: "#333333",
					discountMargin: ["3", "0", "0", "0"],
					discountPadding: ["2", "3", "2", "3"],
					discountCornerRadius: ["5", "5", "5", "5"],
					discountAlign: "center",
					discountBgColor: "#fff000",
					couponDescFontSize: "12",
					couponDescFontColor: "#FFFFFF",
					couponDescFontWeight: "normal",
					couponDescAlign: "center",
					couponImgUrl: "https://babel.m.jd.com/active/babelTower/images/custom_coupon_r_2.png",//一图：https://babel.m.jd.com/active/babelTower/images/custom_coupon_r_1.png 二图：https://babel.m.jd.com/active/babelTower/images/custom_coupon_r_2.png 三图：https://babel.m.jd.com/active/babelTower/images/custom_coupon_r_3.png
					receiveCouponFontSize: "12",
					receiveCouponFontColor: "#fff000",
					receiveCouponFontWeight: "normal",
					tips: "点击领取",
				}
			},
			items: [{
				type: "coupon",
				id: "3501295035",
                dataIds: null,
                name: "",
                desc: "",
                pictureUrl: "",
                sparePictureUrl: null,
                jumpLinkInfo: {
                    linkType: null,
                    linkUrl: "27651488",
                    spareLinkUrl: null,
                    routerUrl: null,
                    pageLinkUrl: null,
                    identityId: null
                },
                hostPictureUrl: null,
                beginTime: "2020-02-04 00:00:00",
                endTime: "2020-02-04 00:00:00",
                price: null,
                jdPrice: null,
                extension: {
                    remark: "29-10",
                    fp: "3ms+zCEJquAuez5NI2K/EA==",
                    inputType: "2",
                    couponId: "27651488",
                    ap: "Rco8/RxD8w0BBlITEyBAiQ==",
                    useCpnType: 0,
                    userLabel: "",
                    userClass: 10000,
                    activityBeginTime: "2020-02-02 15:32:00",
                    expireType: 5,
                    venderId: 10114811,
                    platformType: 0,
                    couponKind: "3",
                    discountDesc: "",
                    batchId: "164048487",
                    couponStyle: 0,
                    overLap: "2",
                    yn: 1,
                    couponType: 1,
                    quota: "29.0",
                    couponKey: "vender_FP#92d05b6543a04ab09c4d6894c2af3b0d",
                    addDays: 0,
                    shopId: 958719,
                    beginTime: "2020-02-02 00:00:00",
                    limitStr: "限购  [魅瞳美妆官方旗舰店] 店铺部分商品",
                    key: "6d2d8c3baedc4e70b6d1ab0ef24b709d",
                    limitOrganization: "[1]",
                    disCount: "10.0",
                    couponInfo: "",
                    roleId: "27651488",
                    cl: [
                        {
                            cn1: "医药保健",
                            c1: "9192"
                        }
                    ],
                    overLapDesc: "[6]",
                    soldOut: 0,
                    activityEndTime: "2020-02-15 23:55:00",
                    hourCoupon: "1",
                    limitPlatform: "",
                    createTime: "2020-02-02 15:41:04",
                    endTime: "2020-02-15 23:59:59",
                    couponTitle: "满29减10",
                    groupId: "03789642",
                    stageId: "10697147",
                    rt: "1",
                    couponValid: 1,
                    cpnResultCode: 200,
                    configStatus: 29,
                    cpnResultMsg: "查询活动成功"
                },
                labelInfos: null,
                userInfo: null,
                assetsItems: null,
                healthCurrency: null,
                hasHealthSteward: false,
                healthSteward: null,
                orderInfo: null,
                trackInfo: null,
                finishOrderInfo: null,
                hot: false
			},
			{
				type: "coupon",
				id: "3501295035",
                dataIds: null,
                name: "",
                desc: "",
                pictureUrl: "",
                sparePictureUrl: null,
                jumpLinkInfo: {
                    linkType: null,
                    linkUrl: "27651488",
                    spareLinkUrl: null,
                    routerUrl: null,
                    pageLinkUrl: null,
                    identityId: null
                },
                hostPictureUrl: null,
                beginTime: "2020-02-04 00:00:00",
                endTime: "2020-02-04 00:00:00",
                price: null,
                jdPrice: null,
                extension: {
                    remark: "29-10",
                    fp: "3ms+zCEJquAuez5NI2K/EA==",
                    inputType: "2",
                    couponId: "27651488",
                    ap: "Rco8/RxD8w0BBlITEyBAiQ==",
                    useCpnType: 0,
                    userLabel: "",
                    userClass: 10000,
                    activityBeginTime: "2020-02-02 15:32:00",
                    expireType: 5,
                    venderId: 10114811,
                    platformType: 0,
                    couponKind: "3",
                    discountDesc: "",
                    batchId: "164048487",
                    couponStyle: 0,
                    overLap: "2",
                    yn: 1,
                    couponType: 1,
                    quota: "29.0",
                    couponKey: "vender_FP#92d05b6543a04ab09c4d6894c2af3b0d",
                    addDays: 0,
                    shopId: 958719,
                    beginTime: "2020-02-02 00:00:00",
                    limitStr: "限购  [魅瞳美妆官方旗舰店] 店铺部分商品",
                    key: "6d2d8c3baedc4e70b6d1ab0ef24b709d",
                    limitOrganization: "[1]",
                    disCount: "10.0",
                    couponInfo: "",
                    roleId: "27651488",
                    cl: [
                        {
                            cn1: "医药保健",
                            c1: "9192"
                        }
                    ],
                    overLapDesc: "[6]",
                    soldOut: 0,
                    activityEndTime: "2020-02-15 23:55:00",
                    hourCoupon: "1",
                    limitPlatform: "",
                    createTime: "2020-02-02 15:41:04",
                    endTime: "2020-02-15 23:59:59",
                    couponTitle: "满29减10",
                    groupId: "03789642",
                    stageId: "10697147",
                    rt: "1",
                    couponValid: 1,
                    cpnResultCode: 200,
                    configStatus: 29,
                    cpnResultMsg: "查询活动成功"
                },
                labelInfos: null,
                userInfo: null,
                assetsItems: null,
                healthCurrency: null,
                hasHealthSteward: false,
                healthSteward: null,
                orderInfo: null,
                trackInfo: null,
                finishOrderInfo: null,
                hot: false
			},
			]
		}
	}, {
		name: "promotion",
		icon: "",
		type: "推广广告",
		tips: "推广广告",
		division: "0",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			bgImg: {
				url: '',
				style: {}
			},
			logoImg: {
				url: '',
				style: {
					width: '',
					top: '',
					left: ''
				}
			},
			textImg: {
				url: '',
				style: {
					width: '',
					top: '',
					left: ''
				}
			},
			textSubImg: {
				url: '',
				style: {
					width: '',
					top: '',
					left: ''
				}
			},
			style: {
				bgColor: "#ffffff",
				margin: ["0", "1", "0", "0"],
				padding: ["0", "10", "0", "10"],
				column: "2",
			},
		}
	},
	{
		name: "couponGift",
		icon: "",
		type: "好礼领取",
		tips: "好礼领取",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			style: {
				bgColor: "#5AC2B3",
				bgImgUrl: "",
				margin: ["0", "0", "0", "0"],
				padding: ["10", "10", "10", "10"],
				cornerRadius: ["0", "0", "0", "0"],
			},
			ext: {
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					margin: ["0", "0", "0", "0"],
					padding: ["10", "10", "10", "10"],
					cornerRadius: ["10", "10", "0", "0"],
				},
			},
			items: []
		}
	}
	]
},
{
	category: "互联网医院",
	id: null,
	list: [{
		name: "quickAccess",
		icon: "",
		type: "疾病百科入口",
		tips: "疾病百科入口",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				row: "2", //
				column: "4",
				cornerRadius: ["0", "0", "0", "0"],
				itemCornerRadiusValue: 5, //装图片的盒子圆角
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "10", "10"],
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "quickAccess",
				style: {
					height: 35,
					display: "inline",
					fontSize: "12",
					fontWeight: "normal",
					bgColor: "#eeeeee",
					titleColor: "#333333",
					marginTop: "8",
					colspan: "",
					reuseId: "",
					picWidth: 40,
					picHeight: 40,
					margin: ["10", "10", "0", "0"],
					itemCornerRadius: ["5", "5", "5", "5"]
				}
			},
			items: [{
				type: "quickAccess",
				name: "甲状腺炎",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "高血压",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "脱发",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: ['//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png'],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "痘痘",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "糖尿病",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "胃炎",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "湿疹",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			},
			{
				type: "quickAccess",
				name: "感冒",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
				},
				sparePictureUrl: [],
				hostPictureUrl: '',
			}
			]
		}
	},
	// {
	//   name: "bannerAccess",
	//   icon: "",
	//   type: "banner入口",
	//   tips: "banner入口",
	//   data: {
	//     type: "40",
	//     style: {
	//       bgColor: "#ffffff",
	//       bgImgUrl: "",
	//       column: "3",
	//       margin: ["0", "0", "0", "0"],
	//       padding: ["0", "0", "10", "0"],
	//       cornerRadius: ["0", "0", "0", "0"]
	//     },
	//     ext: {
	//       dataIds: {},
	//       pageNo: 1,
	//       pageSize: 10,
	//       dataSourceType1: "301",
	//       dataSourceType2: "301301",
	//       type: "enreyCube",
	//       style: {
	//         height: 91,
	//         bgColor: "#eeeeee",
	//         display: "inline",
	//         fontSize: "16",
	//         subTitleShow: true,
	//         fontWeight: "normal",
	//         titleColor: "#333333",
	//         subFontSize: "12",
	//         subTitleColor: "#aaaaaa",
	//         picWidth: 50,
	//         picHeight: 50,
	//         marginTop: "8",
	//         margin: ["10", "0", "0", "0"],
	//         itemCornerRadius: ["0", "0", "0", "0"]
	//       }
	//     },
	//     items: [{
	//         type: "enreyCube",
	//         name: "急速问诊",
	//         jumpLinkInfo: {
	//           linkType: "0", //跳转类型
	//           identityId: "", //跳转页面的链接
	//           linkUrl: "",
	//         },
	//         pictureUrl: '//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png',
	//         desc: "全职医生急速答复"
	//       },
	//       {
	//         type: "enreyCube",
	//         name: "找医生",
	//         jumpLinkInfo: {
	//           linkType: "0", //跳转类型
	//           identityId: "", //跳转页面的链接
	//           linkUrl: "",
	//         },
	//         pictureUrl: '//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png',
	//         desc: "368+三甲医院名医"

	//       },
	//       {
	//         type: "enreyCube",
	//         name: "开药门诊",
	//         jumpLinkInfo: {
	//           linkType: "0", //跳转类型
	//           identityId: "", //跳转页面的链接
	//           linkUrl: "",
	//         },
	//         pictureUrl: "//img11.360buyimg.com/jdphoto/s80x80_jfs/t27847/91/107794072/6854/14716732/5b850ecaN644d2983.png",
	//         desc: "正品药极速达"
	//       }
	//     ]
	//   }
	// }, 
	{
		name: "doctorTeam",
		icon: "",
		type: "医生团队",
		tips: "医生团队",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"]
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "doctorTeam",
				style: {
					bgColor: "#45b97c",
					itemHeight: 106,
					aspectRatio: 3.538,
					margin: [
						"0",
						"0",
						"0",
						"0"
					]
				}
			},
			items: [{
				type: "doctorTeam",
				pictureUrl: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					spareLinkUrl: [],
				},

			}]
		}
	},
	{
		name: "classifyGoods",
		icon: "",
		type: "分类商品",
		tips: "分类商品",
		column: "2",
		scroll: "scroll",
		dataSource: "manual",
		data: [
			// dataSource: "manual",
			// items: []
			//tab切换
			{
				floorBuryPoint: "",
				type: "stickyOff",
				id: "",
				style: {
					freeOffset: "", //???
					bgColor: "",
					margin: ["0", "0", "0", "0"],
					padding: ["0", "0", "0", "0"],
					column: "1", //???
				},
				items: [{
					type: "scrollMenu",
					action: [],
					nextLayoutId: "", //???
					style: {
						reuseId: "scrollMenu - 2",
						bgColor: "#FFFFFF",
						titles: [],
						titleColorNormal: "#999999",
						titleColorSelected: "#333333",
						titleSizeNormal: 16,
						titleSizeSelected: 18,
						selectedIndex: 0
					}
				}]
			},
			//图片+商品
			{
				floorBuryPoint: "",
				type: "40",
				id: "",
				style: {
					bgColor: "",
					padding: ["0", "0", "0", "0"],
					column: "3", //???
				},
				items: [
					{
						type: "image",
						imgUrl: "https://m.360buyimg.com/babel/s778x586_jfs/t1/78731/3/11285/38078/5d8c7268E3e017608/deab2644e1642a60.jpg!q70.dpg",
						action: "inner-ad",
						style: {
							height: "200",
							margin: ["5", "5", "5", "5"],
							padding: ["0", "0", "0", "0"]
						}
					},
					{
						type: "product",
						productImgurl: "https://m.360buyimg.com/babel/s579x579_jfs/t1/38600/26/11389/123678/5d2bd9bfE6e28202b/7b13a8261791b896.jpg!cc_270x270!q70.dpg",
						descriptionText: "",
						price: "199",
						shoppingIcon: "https://m.360buyimg.com/babel/jfs/t1/46087/33/10283/1740/5d75b1e4Ef64124cc/3d126a464fc7864d.png",
						detailAction: "router//detail8",
						shopAction: "router//shop8",
						style: {
							height: "190",
							margin: ["0", "10", "0", "10"]
						}
					},
				]
			}
		]
	},
	]
},
{
	category: "京东APP-我的",
	id: null,
	list: [{
		name: "myService",
		icon: "",
		type: "我的服务入口",
		tips: "我的服务入口",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/88506/33/9789/6488/5e19847eE1fc8cdf4/b3b2fe3412b76ac0.png",
				column: "2",
				cornerRadius: ["0", "0", "0", "0"],
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {},
				pageNo: 1,
				pageSize: 10,
				dataSourceType1: "301",
				dataSourceType2: "301301",
				type: "myService",
				style: {
					height: 105, //item的高度
					margin: ["20", "20", "14", "25"],
					fontSize: "14",
					titleColor: "#262626",
					contentFontSize: "12",
					contentTitleColor: "#8c8c8c"
				}
			},
			items: [{
				type: "myService",
				iconInfo: { //上半部分内容
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/90048/25/10279/11466/5e19829aE8e1c882b/39f43cf8224e476b.png", //上半部分内容的图标
					name: "网上医院",
					arrowImgUrl: "https://m.360buyimg.com/pop/jfs/t1/101275/35/10183/414/5e19837bE4f276d14/c41ed4eaa96a8835.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: { //上半部分内容的样式
						fontSize: "14",
						titleColor: "#262626",
						picWidth: 42,
						picHeight: 42,
						margin: ["0", "0", "0", "0"],
					}
				},
				containerList: [{ //下半部分内容
					name: "开药门诊",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/96975/10/10360/14549/5e1982c0Ec0b90cb3/3e0c6e05f316bfbc.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					desc: "",
					style: { //下半部分内容的样式
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 35,
						picHeight: 35,
						margin: ["8", "0", "8", "0"],
					}
				}, {
					name: "三甲名医",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/86748/34/10196/13736/5e1982dcEb78bf3f9/c54926ea6851a0a7.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					desc: "",
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 35,
						picHeight: 35,
						margin: ["8", "0", "8", "0"],
					}
				}]
			},
			{
				type: "myService",
				iconInfo: {
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/109193/4/4161/10344/5e198245E7932dd8d/d6835cc9cb63059c.png",
					name: "健康大药房",
					arrowImgUrl: "https://m.360buyimg.com/pop/jfs/t1/101275/35/10183/414/5e19837bE4f276d14/c41ed4eaa96a8835.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						fontSize: "14",
						titleColor: "#262626",
						picWidth: 42,
						picHeight: 42,
						margin: ["0", "0", "0", "0"],
					}
				},
				containerList: [{
					name: "快速购药",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/97200/16/10349/1188/5e1982f1E801ac0f6/9afa9d2cf6fe2f52.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}

				},
				{
					name: "滋补养生",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}, {
					name: "营养保健",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}, {
					name: "医保购药",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/97200/16/10349/1188/5e1982f1E801ac0f6/9afa9d2cf6fe2f52.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}
				]
			}, {
				type: "myService",
				iconInfo: {
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/109193/4/4161/10344/5e198245E7932dd8d/d6835cc9cb63059c.png",
					name: "健康服务",
					arrowImgUrl: "https://m.360buyimg.com/pop/jfs/t1/101275/35/10183/414/5e19837bE4f276d14/c41ed4eaa96a8835.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						fontSize: "14",
						titleColor: "#262626",
						picWidth: 42,
						picHeight: 42,
						margin: ["0", "0", "0", "0"],
					}
				},
				containerList: [{
					name: "体检管家",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/97200/16/10349/1188/5e1982f1E801ac0f6/9afa9d2cf6fe2f52.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				},
				{
					name: "基因检测",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}, {
					name: "医学美容",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}, {
					name: "口腔疫苗",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/97200/16/10349/1188/5e1982f1E801ac0f6/9afa9d2cf6fe2f52.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}
				]
			}, {
				type: "myService",
				iconInfo: {
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/90048/25/10279/11466/5e19829aE8e1c882b/39f43cf8224e476b.png",
					name: "健康工具",
					arrowImgUrl: "https://m.360buyimg.com/pop/jfs/t1/101275/35/10183/414/5e19837bE4f276d14/c41ed4eaa96a8835.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						fontSize: "14",
						titleColor: "#262626",
						picWidth: 42,
						picHeight: 42,
						margin: ["0", "0", "0", "0"],
					}
				},
				containerList: [{
					name: "健康打卡",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/97200/16/10349/1188/5e1982f1E801ac0f6/9afa9d2cf6fe2f52.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				},
				{
					name: "健康测评",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}, {
					name: "疾病百科",
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/103450/4/10443/1192/5e198329E167be897/b2fcb4f9132f0d4d.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						linkUrl: "",
					},
					style: {
						contentFontSize: "12",
						contentTitleColor: "#8c8c8c",
						picWidth: 7,
						picHeight: 7,
						margin: ["15", "0", "0", "0"],
					}
				}
				]
			}
			]
		},
	},
	{
		name: "myInfo",
		icon: "",
		type: "我的",
		tips: "我的",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#f4f4f4",
				bgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/100381/29/9811/34011/5e133206Eb5178604/1e9ca586cfb3474f.png",
				column: "1",
				cornerRadius: ["0", "0", "0", "0"],
				margin: ["0", "0", "0", "0"],
				padding: ["0", "0", "0", "0"],
			},
			ext: {
				dataIds: {
					loadType: "3",
					advertId: "",
					stage: 0
				},
				dataSourceId: "", //groupId
				dataSourceType1: "", //数据来源 ace 写手后台
				dataSourceType2: "501001", //数据来源下的数据类型 广告 商品
				type: "IdinforHead"
			},
			items: [{
				type: "IdinforHead",
            userInfo: null,
            assetsItems: [{
                name: "京豆",
                numValueStr: "0",
                jumpLinkInfo: {
                  linkType: "0", //跳转类型
                  identityId: "", //跳转页面的链接
                  routerUrl: "", //跳转页面的链接
                  linkUrl: "https://wqs.jd.com/my/jingdou/my.shtml?sceneval=2&ptag=7155.1.17"
                },
              },
              {
                name: "优惠券",
                numValueStr: "0",
                jumpLinkInfo: {
                  linkType: "0", //跳转类型
                  identityId: "", //跳转页面的链接
                  routerUrl: "", //跳转页面的链接
                  linkUrl: "https://wqs.jd.com/my/coupon/index.shtml?sceneval=2"
                },
              },
              {
                name: "红包",
                numValueStr: "0",
                jumpLinkInfo: {
                  linkType: "0", //跳转类型
                  identityId: "", //跳转页面的链接
                  routerUrl: "", //跳转页面的链接
                  linkUrl: "https://wqs.jd.com/my/coupon/index.shtml?sceneval=2"
                },
              },
            ],
            healthCurrency: null,
            hasHealthSteward: false,
				healthSteward: {
					pictureUrl: "https://m.360buyimg.com/pop/jfs/t1/87152/8/9842/56808/5e13dcaeEb1534170/3502e68af2064236.png",
					jumpLinkInfo: {
						linkType: "0", //跳转类型
						identityId: "", //跳转页面的链接
						routerUrl: "", //跳转页面的链接
						linkUrl: ""
					},
				},
				style: {
					height: "188",
					display: "inline",
					fontColor: "#333333",
					reuseId: "",
					margin: ["70", "20", "0", "20"]
				}
			}]
		},
	},
	//   {
	//     name: "blank",
	//     icon: "",
	//     type: "占位",
	//     tips: "占位",
	//     data: {
	//       type: "40",
	//       style: {
	//         bgColor: "#FFFFFF",
	//         bgImgUrl: "",
	//         margin: ["0", "0", "0", "0"],
	//         padding: ["0", "0", "0", "0"],
	//         cornerRadius: ["0", "0", "0", "0"]
	//       },
	//       ext: {},
	//       items: [{
	//         type: "Apollo",
	//         style: {}
	//       }]
	//     }
	//   },
	{
		name: "myAvatar",
		icon: "",
		type: "头像",
		tips: "头像",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "title-line",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["15", "5", "0", "5"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["6", "6", "0", "0"]
			},
			ext: {
				type: "HdHeadInfor",
				dataSourceId: "", //groupId
				dataSourceType1: "", //数据来源 ace 写手后台
				readCache: "1",
			},
			items: [{
				type: "HdHeadInfor",
				id: "",
				readCacheField: "",
				name: "",
				titleFontSize: "18",
				titleColor: "#333333",
				headImgUrl: "",
				headImgWidth: "60",
				headImgHeight: "60",
				headImgCornerRadius: ["100", "100", "100", "100"],
				account: "", //nickname 
				subFontSize: "13",
				subTitleColor: "#919191",
				gendar: "",
				arrowUrl: "https://m.360buyimg.com/pop/jfs/t1/94532/26/9898/289/5e140f26Eab558844/4e1fb2c87fc3528c.png",
				bottomLine: "1", //1代表有底纹线
				borderColor: "#F2F2F2",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				dataType: "0", //0 头像 1
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					margin: ["15", "15", "15", "15"],
					headMargin: ["0", "0", "10", "0"],
					padding: ["0", "0", "0", "0"],
					colspan: "1",
					height: "100",
					display: "inline",
					reuseId: "",
				}
			}]
		}
	},
	{
		name: "myItem",
		icon: "",
		type: "设置信息",
		tips: "设置信息",
		arrow: "1",
		desc: "0",
		aboutTips: "0",
		border: "1",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "title-line",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["0", "5", "5", "5"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["0", "0", "0", "0"]
			},
			ext: {
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					margin: ["0", "15", "0", "15"],
					padding: ["0", "0", "0", "0"],
					cornerRadius: ["0", "0", "0", "0"],
					height: "60",
					display: "inline",
					colspan: "1",
					reuseId: "",
				},
				type: "HDSettingItem",
				readCache: "1",
			},
			items: [{
				name: "地址管理",
				arrowUrl: "https://m.360buyimg.com/pop/jfs/t1/94532/26/9898/289/5e140f26Eab558844/4e1fb2c87fc3528c.png",
				content: "",
				titleFontSize: "15",
				titleFontColor: "#262626",
				descFontSize: "12",
				descFontColor: "#BABABA",
				tipsFontSize: "14",
				tipsFontColor: "#7D7D7D",
				bottomLine: "1", //1代表有底纹线
				borderColor: "#F2F2F2",
				dataType: "",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
			}]
		}
	},
	{
		name: "myButton",
		icon: "",
		type: "按钮",
		tips: "按钮",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				margin: ["15", "5", "15", "5"],
				padding: ["0", "0", "0", "0"],
				cornerRadius: ["20", "20", "20", "20"],
			},
			ext: {
				type: "text",
			},
			items: [{
				text: "退出登录",
				fontSize: "15",
				textColor: "#262626",
				jumpLinkInfo: {
					linkType: "0", //跳转类型
					identityId: "", //跳转页面的链接
					linkUrl: "",
					routerUrl: ""
				},
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					height: "40",
					display: "inline",
					colspan: "1",
					reuseId: "",
					margin: ["0", "0", "0", "0"],
					padding: ["0", "0", "0", "0"],
					align: "center"
				}
			}]
		}
	},
	{
		name: "orderState",
		icon: "",
		type: "订单状态",
		tips: "订单状态",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "",
			style: {
				bgColor: "#ffffff",
				bgImgUrl: "",
				column: "4",
				cornerRadius: ["10", "10", "0", "0"],
				margin: ["0", "10", "0", "10"],
				padding: ["0", "0", "0", "0"],
			},
			ext: {
				dataSourceType1: "", //数据来源 ace 写手后台
				dataSourceType2: "", //数据来源下的数据类型 广告 商品
				dataSourceId: "", //groupId
				type: "enreyCube",
				style: {
					marginTop: "5",
					picHeight: "25",
					height: "43",
					display: "inline",
					colspan: "1",
					reuseId: "",
					fontSize: "12",
					fontColor: "#000000",
					margin: ["25", "0", "15", "0"],
					imageMultiple: "2",
					imagePositon: "right",//left right 
					seperateImgUrl: "https://m.360buyimg.com/pop/jfs/t1/92112/6/10156/1184/5e195a53E2af108e5/8ea71c5076cd0e57.png",
					dotBulgeTop: "-3",
					dotBulgeRight: "-14",
					dotPadding: ["1", "3", "1", "3"],
					dotBorderSize: "1",
					dotBorderColor: "#F86543",
					dotBgColor: "#F86543",
					dotCornerRadius: ["6", "6", "6", "6"],
					dotFontSize: "10",
					dotFontWeight: "normal",
					dotFontColor: "#FFFFFF",
				},
			},
			items: [
				// {
				// 	name: "待付款",
				// 	pictureUrl: "http://storage.jd.com/jdhapp/image/order/%E5%BE%85%E6%94%B6%E8%B4%A7.png",
				// 	jumpLinkInfo: {
				// 		linkType: "0", //跳转类型
				// 		identityId: "", //跳转页面的链接
				// 		linkUrl: "",
				// 		routerUrl: ""
				// 	},
				// 	style: {
				// 		marginTop: "5",
				// 		picHeight: "25",
				// 		height: "43",
				// 		display: "inline",
				// 		colspan: "1",
				// 		reuseId: "",
				// 		fontSize: "12",
				// 		margin: ["25", "0", "15", "0"],
				// 	}
				// },
				// {
				// 	name: "待收货",
				// 	pictureUrl: "http://storage.jd.com/jdhapp/image/order/%E5%85%A8%E9%83%A8%E8%AE%A2%E5%8D%95.png",
				// 	jumpLinkInfo: {
				// 		linkType: "0", //跳转类型
				// 		identityId: "", //跳转页面的链接
				// 		linkUrl: "",
				// 		routerUrl: ""
				// 	},
				// 	style: {
				// 		marginTop: "5",
				// 		picHeight: "25",
				// 		height: "43",
				// 		display: "inline",
				// 		colspan: "1",
				// 		reuseId: "",
				// 		fontSize: "12",
				// 		margin: ["25", "0", "15", "0"],
				// 	}
				// },
				// {
				// 	name: "待评价",
				// 	pictureUrl: "http://storage.jd.com/jdhapp/image/order/%E5%BE%85%E4%BB%98%E6%AC%BE.png",
				// 	jumpLinkInfo: {
				// 		linkType: "0", //跳转类型
				// 		identityId: "", //跳转页面的链接
				// 		linkUrl: "",
				// 		routerUrl: ""
				// 	},
				// 	style: {
				// 		marginTop: "5",
				// 		picHeight: "25",
				// 		height: "43",
				// 		display: "inline",
				// 		colspan: "1",
				// 		reuseId: "",
				// 		fontSize: "12",
				// 		margin: ["25", "0", "15", "0"],
				// 	}
				// },
				// {
				// 	name: "全部订单",
				// 	pictureUrl: "http://storage.jd.com/jdhapp/image/order/%E5%BE%85%E8%AF%84%E4%BB%B7.png",
				// 	orderInfo: {
				// 		orderState: "waiting4Receipt",
				// 		orderNum: 10
				// 	},
				// 	jumpLinkInfo: {
				// 		linkType: "0", //跳转类型
				// 		identityId: "", //跳转页面的链接
				// 		linkUrl: "",
				// 		routerUrl: ""
				// 	},
				// 	style: {
				// 		marginTop: "5",
				// 		picHeight: "25",
				// 		height: "43",
				// 		display: "inline",
				// 		colspan: "1",
				// 		reuseId: "",
				// 		fontSize: "12",
				// 		margin: ["25", "0", "15", "0"],
				// 		seperateImgUrl: "https://m.360buyimg.com/pop/jfs/t1/92112/6/10156/1184/5e195a53E2af108e5/8ea71c5076cd0e57.png",
				// 	}
				// },
			]
		}
	},
	{
		name: "logistics",
		icon: "",
		type: "物流状态",
		tips: "物流状态",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "container-banner",
			id: "title-line",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "",
				column: "1",
				cornerRadius: ["0", "0", "10", "10"],
				margin: ["0", "10", "10", "10"],
				padding: ["0", "15", "17", "15"],
				indicatorStyle: "stripe",
				hasIndicator: "true",
				indicatorRadius: "2",
				indicatorPosition: "inside",
				align: "right",
				defaultIndicatorColor: "#E7E7E7",
				indicatorColor: "#BABABA",
				autoScrollTimeInterval: "2000",
				autoScroll: "1",
				infiniteMinCount: "1",
				pageHeight: "70",
			},
			ext: {
				type: "orderStateTip",
				style: {
					bgColor: "#F7F7F7",
					display: "inline",
					reuseId: "",
					margin: ["10", "10", "18", "10"],
					padding: ["0", "0", "0", "0"],
					titleFontSize: "13",
					titleFontColor: "#262626",
					subtitleFontSize: "12",
					subtitleFontColor: "#7D7D7D",
					cornerRadius: ["5", "5", "5", "5"],
					pictureMargin: ["10", "10", "0", "10"],
					pictureWidth: "42",
					pictureHeight: "42"
				},
				dataIds: {},
				dataSourceType1: "", //数据来源 ace 写手后台
				dataSourceType2: "501003", //数据来源下的数据类型 广告 商品
			},
			items: []
		}
	}
	]
},
{
	category: "京东APP-完成页",
	id: null,
	list: [{
		name: "accountOrder",
		icon: "",
		type: "订单完成",
		tips: "订单完成",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "title-line",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/95557/39/10653/372612/5e1c003bE923a12f2/1f915cf5eeb40575.png",
				column: "1",
				cornerRadius: ["0", "0", "0", "0"],
				margin: ["0", "0", "0", "0"],
				padding: ["68", "50", "30", "50"],
				pageHeight: "262",
			},
			ext: {
				type: "pay-complete",
				dataSourceType2: "601001",
				dataIds: {
					orderId: "123"
				},
				personId: "123",
				style: {
					height: "165",
					display: "inline",
					bgColor: "#00FFFFFF",
					reuseId: "",
					margin: ["0", "0", "0", "0"],
					payCompleteImageWidth: "100",
					payCompleteImageHeight: "99",
					orderTextButtonFontSize: "12",
					orderTextButtonColor: "#FFFFFF",
					orderTextButtonHeight: "35",
					orderTextButtonWidth: "125",
					orderTextButtonBgcolor: "#00FFFFFF",
					orderTextButtonBorderColor: "#FFFFFF",
					orderTextButtonCornerRadius: ["19", "19", "19", "19"],
					tipsButtonFontSize: "12",
					tipsButtonColor: "#F2270C",
					tipsButtonHeight: "35",
					tipsButtonWidth: "125",
					tipsButtonBgcolor: "#FFFFFF",
					tipsButtonBorderColor: "",
					tipsButtonCornerRadius: ["19", "19", "19", "19"],
					stateImgUrl: "https://m.360buyimg.com/pop/jfs/t1/98512/23/10576/86877/5e1d220bEf1a495ca/73c28cfdc3385f8c.png"
				}
			},
			items: [{}]
		}
	},
	{
		name: "pharmacyTitle",
		icon: "",
		type: "商品标签",
		tips: "商品标签",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "",
			style: {
				bgColor: "#FFFFFF",
				bgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/93166/31/11916/1149/5e3cf0a4E2ddfb4a2/908d1b741b05da63.png",
				column: "2",
				cornerRadius: ["8", "8", "0", "0"],
				margin: ["0", "12", "0", "12"],
				padding: ["0", "0", "0", "0"],
			},
			ext: {
				dataSourceType1: "", //数据来源 ace 写手后台
				dataSourceType2: "", //数据来源下的数据类型 广告 商品
				dataSourceId: "", //groupId
				type: "group-title",
			},
			items: [
				{
					type: "group-title",
					title: "今日低价",
					titleLabelShow: "true",
					titleLabel: "直降",
					subTitle: "为您严选健康精品",
					style: {
						bgColor: "#FFFFFF",
						bgImgUrl: "",
						margin: ["0", "0", "0", "0"],
						padding: ["13", "12", "8", "12"],
						cornerRadius: ["0", "0", "0", "0"],
						titleFontSize: "15",
						titleFontColor: "#0F1319",
						titleFontWeight: "bold",
						titleAlign: "left",
						// titleLabelFontSize: "10",
						// titleLabelFontColor: "#FFFFFF",
						// titleFontWeight: "normal",
						// titleLabelPadding: ["2", "7", "2", "5"],
						// titleLabelMargin: ["0", "0", "0", "5"],
						titleLabelBgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/86302/38/11663/5836/5e399250E6afc6d21/d053c9927ce75fd6.png",
						titleLabelWidth: "33",
						titleLabelHeight: "15",
						titleMargin: ["0", "0", "7", "0"],
						subTitleFontSize: "12",
						subTitleFontColor: "#8C8C8C",
						subTitleFontWeight: "normal",
						subTitleBgColor: "#FFFFFF",
						subTitleAlign: "left",
						subTitleCornerRadius: ["0", "0", "0", "0"],
						subTitleMargin: ["0", "0", "11", "0"],
						subTitlePadding: ["0", "0", "0", "0"],
						borderSize: ["0", "1", "1", "0"],
						borderStyle: ["solid", "solid", "solid", "solid"],//dotted点状 double双线 dashed虚线
						borderColor: ["#EFF0F0", "#EFF0F0", "#EFF0F0", "#EFF0F0"],
						height: ""
					}
				},
				{
					type: "group-title",
					title: "防控用品",
					titleLabelShow: "false",
					titleLabel: "",
					subTitle: "抗击疫情 保障供应",
					style: {
						bgColor: "#FFFFFF",
						bgImgUrl: "",
						margin: ["0", "0", "0", "0"],
						padding: ["13", "12", "8", "12"],
						cornerRadius: ["0", "0", "0", "0"],
						titleFontSize: "15",
						titleFontColor: "#0F1319",
						titleFontWeight: "bold",
						titleAlign: "left",
						titleLabelBgImgUrl: "https://m.360buyimg.com/pop/jfs/t1/86302/38/11663/5836/5e399250E6afc6d21/d053c9927ce75fd6.png",
						titleLabelWidth: "33",
						titleLabelHeight: "15",
						titleMargin: ["0", "0", "7", "0"],
						subTitleFontSize: "12",
						subTitleFontColor: "#8C8C8C",
						subTitleFontWeight: "normal",
						subTitleBgColor: "#FFFFFF",
						subTitleAlign: "left",
						subTitleCornerRadius: ["0", "0", "0", "0"],
						subTitleMargin: ["0", "0", "11", "0"],
						subTitlePadding: ["0", "0", "0", "0"],
						borderSize: ["0", "1", "1", "0"],
						borderStyle: ["solid", "solid", "solid", "solid"],//dotted点状 double双线 dashed虚线
						borderColor: ["#EFF0F0", "#EFF0F0", "#EFF0F0", "#EFF0F0"],
						height: ""
					}
				}
			]
		}
	},
	{
		name: "pharmacy",
		icon: "",
		type: "优质商品",
		tips: "优质商品",
		id: null,
		prodectId: "",
		pageId: "",
		pageIdentityId: "",
		data: {
			floorBuryPoint: "",
			type: "40",
			id: "",
			style: {
				bgColor: "#ffffff",
				column: "4",
				cornerRadius: ["0", "0", "8", "8"],
				margin: ["0", "12", "0", "12"],
				padding: ["0", "0", "0", "0"],
				height: "75",
			},
			ext: {
				dataSourceType1: "", //数据来源 ace 写手后台
				dataSourceType2: "", //数据来源下的数据类型 广告 商品
				dataSourceId: "", //groupId
				type: "",
				style: {
					bgColor: "#FFFFFF",
					bgImgUrl: "",
					margin: ["0", "0", "0", "0"],
					padding: ["0", "12", "13", "12"],
					cornerRadius: ["0", "0", "0", "0"],
					productWidth: "61",
					productHeight: "61",
					display: "inline",
					colspan: "1",
					priceDisplay: "block",
					jdPriceDisplay: "block",
					jdPriceShow: "true",
					shopShow: "false",
					carBgImgUrl: "https://m.360buyimg.com/babel/jfs/t1/50981/28/10064/1740/5d75af08E11017a46/197ea492e3d5d82c.png",
				}
			},
			items: [
				{
					type: "product",
					id: "1458321416",
					dataIds: null,
					name: "东阿阿胶 桃花姬阿胶糕ejiao阿胶膏即食阿胶固元膏75gx4礼盒装JC 女性滋补品 健康礼盒",
					desc: "",
					pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/90660/20/11435/112600/5e366176Edcf39811/f3db9b95d2e5e51b.jpg",
					sparePictureUrl: null,
					hostPictureUrl: null,
					beginTime: null,
					endTime: null,
					price: "324.00",
					jdPrice: "464.00",
					labelInfos: null,
					userInfo: null,
					assetsItems: null,
					healthCurrency: null,
					hasHealthSteward: false,
					healthSteward: null,
					orderInfo: null,
					trackInfo: null,
					finishOrderInfo: null,
					hot: false,
					jumpLinkInfo: {
						linkType: null,
						linkUrl: null,
						spareLinkUrl: null,
						routerUrl: "1458321416",
						pageLinkUrl: null,
						identityId: null
					},
				},
				{
					type: "product",
					id: "",
					id: "1458321416",
					dataIds: null,
					name: "东阿阿胶 桃花姬阿胶糕ejiao阿胶膏即食阿胶固元膏75gx4礼盒装JC 女性滋补品 健康礼盒",
					desc: "",
					pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/90660/20/11435/112600/5e366176Edcf39811/f3db9b95d2e5e51b.jpg",
					sparePictureUrl: null,
					hostPictureUrl: null,
					beginTime: null,
					endTime: null,
					price: "324.00",
					jdPrice: "464.00",
					labelInfos: null,
					userInfo: null,
					assetsItems: null,
					healthCurrency: null,
					hasHealthSteward: false,
					healthSteward: null,
					orderInfo: null,
					trackInfo: null,
					finishOrderInfo: null,
					hot: false,
					jumpLinkInfo: {
						linkType: null,
						linkUrl: null,
						spareLinkUrl: null,
						routerUrl: "1458321416",
						pageLinkUrl: null,
						identityId: null
					},
				},
				{
					type: "product",
					id: "",
					id: "1458321416",
					dataIds: null,
					name: "东阿阿胶 桃花姬阿胶糕ejiao阿胶膏即食阿胶固元膏75gx4礼盒装JC 女性滋补品 健康礼盒",
					desc: "",
					pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/90660/20/11435/112600/5e366176Edcf39811/f3db9b95d2e5e51b.jpg",
					sparePictureUrl: null,
					hostPictureUrl: null,
					beginTime: null,
					endTime: null,
					price: "324.00",
					jdPrice: "464.00",
					labelInfos: null,
					userInfo: null,
					assetsItems: null,
					healthCurrency: null,
					hasHealthSteward: false,
					healthSteward: null,
					orderInfo: null,
					trackInfo: null,
					finishOrderInfo: null,
					hot: false,
					jumpLinkInfo: {
						linkType: null,
						linkUrl: null,
						spareLinkUrl: null,
						routerUrl: "1458321416",
						pageLinkUrl: null,
						identityId: null
					},
				},
				{
					type: "product",
					id: "",
					id: "1458321416",
					dataIds: null,
					name: "东阿阿胶 桃花姬阿胶糕ejiao阿胶膏即食阿胶固元膏75gx4礼盒装JC 女性滋补品 健康礼盒",
					desc: "",
					pictureUrl: "https://m.360buyimg.com/babel/jfs/t1/90660/20/11435/112600/5e366176Edcf39811/f3db9b95d2e5e51b.jpg",
					sparePictureUrl: null,
					hostPictureUrl: null,
					beginTime: null,
					endTime: null,
					price: "324.00",
					jdPrice: "464.00",
					labelInfos: null,
					userInfo: null,
					assetsItems: null,
					healthCurrency: null,
					hasHealthSteward: false,
					healthSteward: null,
					orderInfo: null,
					trackInfo: null,
					finishOrderInfo: null,
					hot: false,
					jumpLinkInfo: {
						linkType: null,
						linkUrl: null,
						spareLinkUrl: null,
						routerUrl: "1458321416",
						pageLinkUrl: null,
						identityId: null
					},
				}
			]
		}
	}
]
}
];

//中间预览区的组件
const componentsPreview = {
  product: products,
  carousel: carousel,
  // floorSpacing: floorSpacing,
  slogan: slogan,
  classify: classify,
  advertisement: advertisement,
  brandSale: brandSale,
  seckill: seckill,
  decorateTitle: decorateTitle,
  //   imgTitle: imgTitle,
  slidesScroll: slidesScroll,
  tabScroll: tabScroll,
  // textLine: textLine,
  bottomTips: bottomTips,
  notice: notice,
  oneToTwo: oneToTwo,
  coupon: coupon,
  search: search,
  promotion: promotion,
  //   stickyLabel: stickyLabel,
  // top: top,
  classifyGoods: classifyGoods,
  tab: tab,
  myService: myService,
  myInfo: myInfo,
  quickAccess: quickAccess,
  //bannerAccess: bannerAccess,
  doctorTeam: doctorTeam,
  doctorScroll: doctorScroll,
  pictureNews: pictureNews,
  doctorNews: doctorNews,
  tabIcon: tabIcon,
  couponGift: couponGift,
  myAvatar: myAvatar,
  myItem: myItem,
  myButton: myButton,
  //blank: blank,
  overlap: overlap,
  orderState: orderState,
  logistics: logistics,
  accountOrder: accountOrder,
  pharmacy: pharmacy,
  pharmacyTitle: pharmacyTitle,
  //hotArea: hotArea
};
//右侧编辑区的组件
const componentEdit = {
	product: productEdit,
	carousel: carouselEdit,
	// floorSpacing: floorSpacingEdit,
	slogan: sloganEdit,
	classify: classifyEdit,
	advertisement: advertisementEdit,
	brandSale: brandSaleEdit,
	seckill: seckillEdit,
	decorateTitle: decorateTitleEdit,
	//   imgTitle: imgTitleEdit,
	slidesScroll: slidesScrollEdit,
	// textLine: textLineEdit,
	bottomTips: bottomTipsEdit,
	notice: noticeEdit,
	oneToTwo: oneToTwoEdit,
	coupon: couponEdit,
	search: searchEdit,
	promotion: promotionEdit,
	//   stickyLabel: stickyLabelEdit,
	// top: topEdit,
	classifyGoods: classifyGoodsEdit,
	tab: tabEdit,
	myService: myServiceEdit,
	myInfo: myInfoEdit,
	quickAccess: quickAccessEdit,
	//bannerAccess: bannerAccessEdit,
	doctorTeam: doctorTeamEdit,
	doctorScroll: doctorScrollEdit,
	pictureNews: pictureNewsEdit,
	doctorNews: doctorNewsEdit,
	tabIcon: tabIconEdit,
	couponGift: couponGiftEdit,
	myAvatar: myAvatarEdit,
	myItem: myItemEdit,
	myButton: myButtonEdit,
	//blank: blankEdit,
	overlap: overlapEdit,
	orderState: orderStateEdit,
	logistics: logisticsEdit,
	accountOrder: accountOrderEdit,
	pharmacy: pharmacyEdit,
	pharmacyTitle: pharmacyTitleEdit,
	//hotArea: hotAreaEdit
};

export {
	componentsPreview,
	componentEdit,
	tags
};
