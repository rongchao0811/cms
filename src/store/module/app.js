import {
    getBreadCrumbList,
    setTagNavListInLocalstorage,
    getMenuByRouter,
    getTagNavListFromLocalstorage,
    getHomeRoute,
    getNextRoute,
    routeHasExist,
    routeEqual,
    getRouteTitleHandled,
    localSave,
    localRead
} from "@/libs/util";
import { saveErrorLogger } from "@/api/data";
import router from "@/router";
import routers from "@/router/routers";
import config from "@/config";

const { homeName } = config;

const closePage = (state, route) => {
    const nextRoute = getNextRoute(state.tagNavList, route);
    state.tagNavList = state.tagNavList.filter(item => {
        return !routeEqual(item, route);
    });
    router.push(nextRoute);
};

export default {
    state: {
        breadCrumbList: [],
        tagNavList: [],
        homeRoute: {},
        local: localRead("local"),
        errorList: [],
        hasReadErrorPage: false,
        floorList: [],
        currentFloorIndex: null,
        selectedTabIndex: 0,
        floorConfigData: [],
        pid: "",
        element: []
    },
    getters: {
        menuList: (state, getters, rootState) =>
            getMenuByRouter(routers, rootState.user.access),
        errorCount: state => state.errorList.length
    },
    mutations: {
        setBreadCrumb(state, route) {
            state.breadCrumbList = getBreadCrumbList(route, state.homeRoute);
        },
        setHomeRoute(state, routes) {
            state.homeRoute = getHomeRoute(routes, homeName);
        },
        setTagNavList(state, list) {
            let tagList = [];
            if (list) {
                tagList = [...list];
            } else tagList = getTagNavListFromLocalstorage() || [];
            if (tagList[0] && tagList[0].name !== homeName) tagList.shift();
            let homeTagIndex = tagList.findIndex(item => item.name === homeName);
            if (homeTagIndex > 0) {
                let homeTag = tagList.splice(homeTagIndex, 1)[0];
                tagList.unshift(homeTag);
            }
            state.tagNavList = tagList;
            setTagNavListInLocalstorage([...tagList]);
        },
        closeTag(state, route) {
            let tag = state.tagNavList.filter(item => routeEqual(item, route));
            route = tag[0] ? tag[0] : null;
            if (!route) return;
            closePage(state, route);
        },
        addTag(state, { route, type = "unshift" }) {
            let router = getRouteTitleHandled(route);
            if (!routeHasExist(state.tagNavList, router)) {
                if (type === "push") state.tagNavList.push(router);
                else {
                    if (router.name === homeName) state.tagNavList.unshift(router);
                    else state.tagNavList.splice(1, 0, router);
                }
                setTagNavListInLocalstorage([...state.tagNavList]);
            }
        },
        setLocal(state, lang) {
            localSave("local", lang);
            state.local = lang;
        },
        addError(state, error) {
            state.errorList.push(error);
        },
        setHasReadErrorLoggerStatus(state, status = true) {
            state.hasReadErrorPage = status;
        },
        setPid(state, v) {
            state.pid = v
        },
        updateFloor(state, data) {
            //添加楼层数据
            if (data.type == "add") {
                //state.floorList.push(data.floor)
                // if (state.currentFloorIndex !== null) {
                //     state.floorList.splice(state.currentFloorIndex + 1, 0, data.floor)
                // } else {
                    state.floorList.push(data.floor)
                // }
            } else if (data.type == "del") {
                state.floorList = state.floorList.filter((item, index) => {
                    return index != data.index;
                });
                console.log("删除后的==" + JSON.stringify(state.floorList));
            } else if (data.type === 'update') {
                _.assign(state.floorList[data.index].data, data.data)
                console.log("更新后的==" + JSON.stringify(state.floorList));
            } else if (data.type === 'addPage') {
                //增加一整个楼层数据，用于编辑
                state.floorList = data.data
            } else if (data.type == 'reset') {
                state.floorList = []
            }
        },
        updatapageData(state, data) {
            if (data.type == 'reset') {
                state.floorConfigData = []
            } else if (data.type == 'add') {
                state.floorConfigData.push(data.data)
            } else if (data.type == 'del') {
                state.floorConfigData = state.floorConfigData.filter((item, index) => {
                    return item.name != data.name;
                });
            } else if (data.type === 'addPage') {
                state.floorConfigData = data.data
            } else if (data.type === 'updata') {
                state.floorConfigData.filter((item, index) => {
                    if (item.name == data.name) {
                        item = data
                    }
                });
            }
        },
        setFlrIndex(state, index) {
            state.currentFloorIndex = index;
        },
        updateSelectedTabIndex(state, index) {
            state.selectedTabIndex = index;
        },
        updateElement(state, data){
            //console.log(data)
            if(data.type === 'add'){
                state.element = []
                state.element.push(data.data)
            }else if(data.type === 'update'){
                state.partType[data.data.indexes].is = 'true'
            }
        }
    },
    actions: {
        addErrorLog({ commit, rootState }, info) {
            if (!window.location.href.includes("error_logger_page"))
                commit("setHasReadErrorLoggerStatus", false);
            const {
                user: { token, userId, userName }
            } = rootState;
            let data = {
                ...info,
                time: Date.parse(new Date()),
                token,
                userId,
                userName
            };
            // saveErrorLogger(info).then(() => {
            //     commit("addError", data);
            // });
        }
    }
};
