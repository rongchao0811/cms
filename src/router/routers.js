import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [{
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [{
      path: '/home',
      name: 'home',
      meta: {
        hideInMenu: true,
        title: '首页',
        notCache: true,
        icon: 'md-home'
      },
      component: () => import('@/view/single-page/home')
    }]
  },
  {
    path: '/activeManage',
    name: '活动管理',
    meta: {
      icon: 'md-tennisball',
      title: '活动管理',
      access: ['/activeList']
    },
    component: Main,
    children: [{
        path: '/activeList',
        name: '活动列表',
        meta: {
          icon: 'md-flag',
          title: '活动管理',
        },
        component: () => import('@/view/activeManage/activeManage.vue')
      },
      {
        path: '/createActive',
        name: '编辑活动',
        meta: {
          icon: 'md-flag',
          hideInMenu: true,
          title: '编辑活动',
          access: ['/createActive']
        },
        component: () => import('@/view/activeManage/createActive.vue')
      },
      // {
      //   path: '/prizesManage',
      //   name: '奖品列表',
      //   meta: {
      //     icon: 'logo-dropbox',
      //     title: '奖品列表'
      //   },
      //   component: () => import('@/view/activeManage/prizesManage.vue')
      // },
      // {
      //   path: '/activeSetting',
      //   name: '活动规则配置',
      //   meta: {
      //     icon: 'md-create',
      //     title: '活动规则配置'
      //   },
      //   component: () => import('@/view/activeManage/activeSetting.vue')
      // },
      // {
      //   path: '/activeSetting',
      //   name: '投放规则配置',
      //   meta: {
      //     icon: 'md-create',
      //     title: '投放规则配置'
      //   },
      //   component: () => import('@/view/activeManage/activeSetting.vue')
      // },
    ]
  },
  {
    path: '/prizes',
    name: '奖品管理',
    meta: {
      icon: 'md-medal',
      title: '奖品管理',
      access: ['/prizes']
    },
    component: Main,
    children: [{
        path: '/prizesManage',
        name: '奖品列表',
        meta: {
          icon: 'logo-dropbox',
          title: '奖品列表'
        },
        component: () => import('@/view/activeManage/prizesManage.vue')
      },
      // {
      //   path: '/prizesRole',
      //   name: '奖品规则配置',
      //   meta: {
      //     icon: 'md-create',
      //     title: '奖品规则配置'
      //   },
      //   component: () => import('@/view/activeManage/activeManage.vue')
      // },
    ]
  },
  {
    path: '/decorationManage',
    name: '楼层装修',
    meta: {
      icon: 'md-construct',
      title: '楼层装修',
      access: ['/decorationManage']
    },
    component: Main,
    children: [{
        path: '/addPage',
        name: '新增页面',
        meta: {
          icon: 'ios-hammer',
          title: '新增页面',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/index.vue')
      },
      {
        path: '/addProject',
        name: '项目列表',
        meta: {
          icon: 'md-apps',
          title: '项目列表'
        },
        component: () => import('@/view/shop-decoration/projects.vue')
      },
      {
        path: '/decorationList',
        name: '页面管理',
        meta: {
          icon: 'md-create',
          title: '页面管理',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/pageList.vue')
      },
      {
        path: '/stages',
        name: '页面分期',
        meta: {
          icon: 'ios-alarm-outline',
          title: '页面分期',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/stages.vue')
      },
      {
        path: '/material',
        name: '素材管理',
        meta: {
          icon: 'ios-hammer',
          title: '素材管理',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/material/material.vue')
      },
      {
        path: '/bypass',
        name: '配置分流',
        meta: {
          icon: 'ios-hammer',
          title: '配置分流',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/common/bypass.vue')
      },
      {
        path: '/navigate',
        name: '导航编辑',
        meta: {
          icon: 'ios-hammer',
          title: '导航编辑',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/navigation/navigateEdit.vue')
      },
      {
        path: '/analysis',
        name: '页面分析',
        meta: {
          icon: 'md-trending-up',
          title: '页面分析'
        },
        component: () => import('@/view/analysis/factorPage.vue')
      },
      {
        path: '/clickEventAnalysis',
        name: '点击事件',
        meta: {
          icon: 'ios-stats-outline',
          title: '点击事件'
        },
        component: () => import('@/view/analysis/clickEventAnalysis.vue')
      },
    ]
  },
  {
    path: '/healthApp',
    name: '健康App',
    meta: {
      icon: 'ios-phone-portrait',
      title: '健康App',
      access: ['/healthApp']
    },
    component: Main,
    children: [{
      path: '/searchManage',
      name: '搜索管理',
      meta: {
        icon: 'ios-search-outline',
        title: '搜索管理'
      },
      component: () => import('@/view/searchManage/list.vue')
    }, {
      path: '/floorManage',
      name: '商详楼层管理',
      meta: {
        icon: 'logo-buffer',
        title: '商详楼层管理'
      },
      component: () => import('@/view/floorManage/index.vue')
    }, ]
  },
  {
    path: '/miniProgram',
    name: '健康小程序',
    meta: {
      icon: 'ios-infinite',
      title: '健康小程序',
      access: ['/miniProgram']
    },
    component: Main,
    children: [{
      path: '/miniVersion',
      name: '小程序版本管理',
      meta: {
        icon: 'ios-browsers',
        title: '小程序版本管理'
      },
      component: () => import('@/view/miniProgram/setVersion.vue')
    }, ]
  },
  {
    path: '/systemManage',
    name: '系统管理',
    meta: {
      icon: 'md-settings',
      title: '系统管理',
      access: ['/systemManage']
    },
    component: Main,
    children: [{
      path: '/dic',
      name: '字典管理',
      meta: {
        icon: 'ios-book-outline',
        title: '字典管理'
      },
      component: () => import('@/view/system/dictionary.vue')
    }, {
      path: '/blackList',
      name: '黑名单管理',
      meta: {
        icon: 'ios-paper-outline',
        title: '黑名单管理'
      },
      component: () => import('@/view/system/blackList.vue')
    }, {
      path: '/whiteList',
      name: '白名单管理',
      meta: {
        icon: 'ios-paper',
        title: '白名单管理'
      },
      component: () => import('@/view/system/whiteList.vue')
    }]
  },
  {
    path: '/userDataManage',
    name: '用户网络关系',
    meta: {
      icon: 'ios-stats',
      title: '用户网络关系',
      access: ['/userDataManage']
    },
    component: Main,
    children: [{
        path: '/relationData',
        name: '用户关系',
        meta: {
          icon: 'ios-git-merge',
          title: '用户关系'
        },
        component: () => import('@/view/dataManage/userRelation.vue')
      },
      {
        path: '/userRadiation',
        name: '用户辐射',
        meta: {
          icon: 'ios-git-merge',
          title: '用户辐射'
        },
        component: () => import('@/view/dataManage/userRelation1.vue')
      },
      {
        path: '/userRedar',
        name: '用户相性',
        meta: {
          icon: 'ios-git-merge',
          title: '用户相性'
        },
        component: () => import('@/view/dataManage/userLabel.vue')
      },
      {
        path: '/TopN',
        name: '星级用户',
        meta: {
          icon: 'ios-git-merge',
          title: '星级用户'
        },
        component: () => import('@/view/dataManage/TOP-N.vue')
      },
      {
        path: '/userGroup',
        name: '社区发现',
        meta: {
          icon: 'ios-git-merge',
          title: '社区发现'
        },
        component: () => import('@/view/dataManage/userGroup.vue')
      },
      {
        path: '/userTable',
        name: '社区信息',
        meta: {
          icon: 'ios-git-merge',
          title: '社区信息',
          hideInMenu: true
        },
        component: () => import('@/view/dataManage/userTable.vue')
      },
    ]
  },
  {
    path: '/operativeManage',
    name: '运营管理',
    meta: {
      icon: 'ios-list-box-outline',
      title: '运营管理',
      access: ['/operativeManage']
    },
    component: Main,
    children: [{
        path: '/editOperatePage',
        name: '编辑页面',
        meta: {
          icon: 'ios-hammer',
          title: '编辑页面',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/index.vue')
      }, {
        path: '/operativeFloor',
        name: '楼层管理',
        meta: {
          icon: 'md-code-working',
          title: '楼层管理'
        },
        component: () => import('@/view/shop-decoration/projects.vue')
      },
      {
        path: '/operativePageList',
        name: '页面列表',
        meta: {
          icon: 'md-create',
          title: '页面列表',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/pageList.vue')
      },
      {
        path: '/operativeData',
        name: '楼层数据配置',
        meta: {
          icon: 'ios-hammer',
          title: '楼层数据配置',
          hideInMenu: true
        },
        component: () => import('@/view/shop-decoration/index.vue')
      },
    ]
  },
  // {
  //   path: '',
  //   name: 'doc',
  //   meta: {
  //     title: '文档',
  //     href: 'https://lison16.github.io/iview-admin-doc/#/',
  //     icon: 'ios-book'
  //   }
  // },
  // {
  //   path: '/join',
  //   name: 'join',
  //   component: Main,
  //   meta: {
  //     hideInBread: true
  //   },
  //   children: [
  //     {
  //       path: 'join_page',
  //       name: 'join_page',
  //       meta: {
  //         icon: '_qq',
  //         title: 'QQ群'
  //       },
  //       component: () => import('@/view/join-page.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/message',
  //   name: 'message',
  //   component: Main,
  //   meta: {
  //     hideInBread: true,
  //     hideInMenu: true
  //   },
  //   children: [
  //     {
  //       path: 'message_page',
  //       name: 'message_page',
  //       meta: {
  //         icon: 'md-notifications',
  //         title: '消息中心'
  //       },
  //       component: () => import('@/view/single-page/message/index.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/components',
  //   name: 'components',
  //   meta: {
  //     icon: 'logo-buffer',
  //     title: '组件'
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'tree_select_page',
  //       name: 'tree_select_page',
  //       meta: {
  //         icon: 'md-arrow-dropdown-circle',
  //         title: '树状下拉选择器'
  //       },
  //       component: () => import('@/view/components/tree-select/index.vue')
  //     },
  //     {
  //       path: 'count_to_page',
  //       name: 'count_to_page',
  //       meta: {
  //         icon: 'md-trending-up',
  //         title: '数字渐变'
  //       },
  //       component: () => import('@/view/components/count-to/count-to.vue')
  //     },
  //     {
  //       path: 'drag_list_page',
  //       name: 'drag_list_page',
  //       meta: {
  //         icon: 'ios-infinite',
  //         title: '拖拽列表'
  //       },
  //       component: () => import('@/view/components/drag-list/drag-list.vue')
  //     },
  //     {
  //       path: 'drag_drawer_page',
  //       name: 'drag_drawer_page',
  //       meta: {
  //         icon: 'md-list',
  //         title: '可拖拽抽屉'
  //       },
  //       component: () => import('@/view/components/drag-drawer')
  //     },
  //     {
  //       path: 'org_tree_page',
  //       name: 'org_tree_page',
  //       meta: {
  //         icon: 'ios-people',
  //         title: '组织结构树'
  //       },
  //       component: () => import('@/view/components/org-tree')
  //     },
  //     {
  //       path: 'tree_table_page',
  //       name: 'tree_table_page',
  //       meta: {
  //         icon: 'md-git-branch',
  //         title: '树状表格'
  //       },
  //       component: () => import('@/view/components/tree-table/index.vue')
  //     },
  //     {
  //       path: 'cropper_page',
  //       name: 'cropper_page',
  //       meta: {
  //         icon: 'md-crop',
  //         title: '图片裁剪'
  //       },
  //       component: () => import('@/view/components/cropper/cropper.vue')
  //     },
  //     {
  //       path: 'tables_page',
  //       name: 'tables_page',
  //       meta: {
  //         icon: 'md-grid',
  //         title: '多功能表格'
  //       },
  //       component: () => import('@/view/components/tables/tables.vue')
  //     },
  //     {
  //       path: 'split_pane_page',
  //       name: 'split_pane_page',
  //       meta: {
  //         icon: 'md-pause',
  //         title: '分割窗口'
  //       },
  //       component: () => import('@/view/components/split-pane/split-pane.vue')
  //     },
  //     {
  //       path: 'markdown_page',
  //       name: 'markdown_page',
  //       meta: {
  //         icon: 'logo-markdown',
  //         title: 'Markdown编辑器'
  //       },
  //       component: () => import('@/view/components/markdown/markdown.vue')
  //     },
  //     {
  //       path: 'editor_page',
  //       name: 'editor_page',
  //       meta: {
  //         icon: 'ios-create',
  //         title: '富文本编辑器'
  //       },
  //       component: () => import('@/view/components/editor/editor.vue')
  //     },
  //     {
  //       path: 'icons_page',
  //       name: 'icons_page',
  //       meta: {
  //         icon: '_bear',
  //         title: '自定义图标'
  //       },
  //       component: () => import('@/view/components/icons/icons.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/update',
  //   name: 'update',
  //   meta: {
  //     icon: 'md-cloud-upload',
  //     title: '数据上传'
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'update_table_page',
  //       name: 'update_table_page',
  //       meta: {
  //         icon: 'ios-document',
  //         title: '上传Csv'
  //       },
  //       component: () => import('@/view/update/update-table.vue')
  //     },
  //     {
  //       path: 'update_paste_page',
  //       name: 'update_paste_page',
  //       meta: {
  //         icon: 'md-clipboard',
  //         title: '粘贴表格数据'
  //       },
  //       component: () => import('@/view/update/update-paste.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/excel',
  //   name: 'excel',
  //   meta: {
  //     icon: 'ios-stats',
  //     title: 'EXCEL导入导出'
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'upload-excel',
  //       name: 'upload-excel',
  //       meta: {
  //         icon: 'md-add',
  //         title: '导入EXCEL'
  //       },
  //       component: () => import('@/view/excel/upload-excel.vue')
  //     },
  //     {
  //       path: 'export-excel',
  //       name: 'export-excel',
  //       meta: {
  //         icon: 'md-download',
  //         title: '导出EXCEL'
  //       },
  //       component: () => import('@/view/excel/export-excel.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/tools_methods',
  //   name: 'tools_methods',
  //   meta: {
  //     hideInBread: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'tools_methods_page',
  //       name: 'tools_methods_page',
  //       meta: {
  //         icon: 'ios-hammer',
  //         title: '工具方法',
  //         beforeCloseName: 'before_close_normal'
  //       },
  //       component: () => import('@/view/tools-methods/tools-methods.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/i18n',
  //   name: 'i18n',
  //   meta: {
  //     hideInBread: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'i18n_page',
  //       name: 'i18n_page',
  //       meta: {
  //         icon: 'md-planet',
  //         title: 'i18n - {{ i18n_page }}'
  //       },
  //       component: () => import('@/view/i18n/i18n-page.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/error_store',
  //   name: 'error_store',
  //   meta: {
  //     hideInBread: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'error_store_page',
  //       name: 'error_store_page',
  //       meta: {
  //         icon: 'ios-bug',
  //         title: '错误收集'
  //       },
  //       component: () => import('@/view/error-store/error-store.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/error_logger',
  //   name: 'error_logger',
  //   meta: {
  //     hideInBread: true,
  //     hideInMenu: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'error_logger_page',
  //       name: 'error_logger_page',
  //       meta: {
  //         icon: 'ios-bug',
  //         title: '错误收集'
  //       },
  //       component: () => import('@/view/single-page/error-logger.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/directive',
  //   name: 'directive',
  //   meta: {
  //     hideInBread: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'directive_page',
  //       name: 'directive_page',
  //       meta: {
  //         icon: 'ios-navigate',
  //         title: '指令'
  //       },
  //       component: () => import('@/view/directive/directive.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/multilevel',
  //   name: 'multilevel',
  //   meta: {
  //     icon: 'md-menu',
  //     title: '多级菜单'
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'level_2_1',
  //       name: 'level_2_1',
  //       meta: {
  //         icon: 'md-funnel',
  //         title: '二级-1'
  //       },
  //       component: () => import('@/view/multilevel/level-2-1.vue')
  //     },
  //     {
  //       path: 'level_2_2',
  //       name: 'level_2_2',
  //       meta: {
  //         access: ['super_admin'],
  //         icon: 'md-funnel',
  //         showAlways: true,
  //         title: '二级-2'
  //       },
  //       component: parentView,
  //       children: [
  //         {
  //           path: 'level_2_2_1',
  //           name: 'level_2_2_1',
  //           meta: {
  //             icon: 'md-funnel',
  //             title: '三级'
  //           },
  //           component: () => import('@/view/multilevel/level-2-2/level-2-2-1.vue')
  //         },
  //         {
  //           path: 'level_2_2_2',
  //           name: 'level_2_2_2',
  //           meta: {
  //             icon: 'md-funnel',
  //             title: '三级'
  //           },
  //           component: () => import('@/view/multilevel/level-2-2/level-2-2-2.vue')
  //         }
  //       ]
  //     },
  //     {
  //       path: 'level_2_3',
  //       name: 'level_2_3',
  //       meta: {
  //         icon: 'md-funnel',
  //         title: '二级-3'
  //       },
  //       component: () => import('@/view/multilevel/level-2-3.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/argu',
  //   name: 'argu',
  //   meta: {
  //     hideInMenu: true
  //   },
  //   component: Main,
  //   children: [
  //     {
  //       path: 'params/:id',
  //       name: 'params',
  //       meta: {
  //         icon: 'md-flower',
  //         title: route => `{{ params }}-${route.params.id}`,
  //         notCache: true,
  //         beforeCloseName: 'before_close_normal'
  //       },
  //       component: () => import('@/view/argu-page/params.vue')
  //     },
  //     {
  //       path: 'query',
  //       name: 'query',
  //       meta: {
  //         icon: 'md-flower',
  //         title: route => `{{ query }}-${route.query.id}`,
  //         notCache: true
  //       },
  //       component: () => import('@/view/argu-page/query.vue')
  //     }
  //   ]
  // },
  // {
  //   path: '/401',
  //   name: 'error_401',
  //   meta: {
  //     hideInMenu: true
  //   },
  //   component: () => import('@/view/error-page/401.vue')
  // },
  // {
  //   path: '/500',
  //   name: 'error_500',
  //   meta: {
  //     hideInMenu: true
  //   },
  //   component: () => import('@/view/error-page/500.vue')
  // },
  // {
  //   path: '*',
  //   name: 'error_404',
  //   meta: {
  //     hideInMenu: true
  //   },
  //   component: () => import('@/view/error-page/404.vue')
  // }
]
