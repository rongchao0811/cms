/**
 * 美化滚动条
 */
import Vue from 'vue'
import PerfectScrollbar from 'perfect-scrollbar'
import 'perfect-scrollbar/css/perfect-scrollbar.css'

/**
 * @description 自动判断该更新PerfectScrollbar还是创建它
 * @param {HTMLElement} el - 必填。dom元素
 * https://blog.csdn.net/weixin_33721344/article/details/88826454
 */
const el_scrollBar = (el) => {
    if (el._ps_ instanceof PerfectScrollbar) {
        el._ps_.update();
    } else {
        el._ps_ = new PerfectScrollbar(el, { suppressScrollX: true });
    }
};

Vue.directive("scrollBar", {
    inserted(el, binding, vnode) {
        const rules = ["fixed", "absolute", "relative"];
        if (!rules.includes(window.getComputedStyle(el, null).position)) {
            console.error(`perfect-scrollbar所在的容器的position属性必须是以下之一：${rules.join("、")}`)
        }
        el_scrollBar(el);
    },
    //更新dom的时候
    componentUpdated(el, binding, vnode, oldVnode) {
        try {
            vnode.context.$nextTick(
                () => {
                    el_scrollBar(el);
                }
            )
        } catch (error) {
            console.error(error);
            el_scrollBar(el);
        }
    }
})