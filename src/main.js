// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import iView from 'iview'
import i18n from '@/locale'
import config from '@/config'
import importDirective from '@/directive'
import {
  directive as clickOutside
} from 'v-click-outside-x'
import installPlugin from '@/plugin'
import axios from '@/libs/api.request'
import './index.less'
import './assets/css/base.less'
import './assets/js/scroll'
import TreeTable from 'tree-table-vue'
import VOrgTree from 'v-org-tree'
import {
  Table,
  TableColumn,
  Button
} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'v-org-tree/dist/v-org-tree.css'
//import VueBus from './view/shop-decoration/config/bus'
// import "font-awesome/css/font-awesome.css";//图标字体
import tools from './assets/js/arms.es6';
import _ from "lodash"

import linkType from "@/view/shop-decoration/common/linkType.vue";
import dataSource from "@/view/shop-decoration/common/dataSource.vue";
import aceAdvert from "@/view/shop-decoration/common/aceAdvert.vue";
import floorTargeted from "@/view/shop-decoration/common/floorTargeted.vue";
import netAdvert from "@/view/shop-decoration/common/netAdvert.vue";
import ImageUpload from "@/view/shop-decoration/common/ImageUpload.vue";
import floorConfig from "@/view/shop-decoration/common/floorStyleConfig.vue";

// 实际打包时应该不引入mock
/* eslint-disable */
if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(iView, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(TreeTable)
Vue.use(TableColumn)
Vue.use(Button)
Vue.use(VOrgTree)
Vue.use(Table)
//Vue.use(VueBus)
Vue.component('linkType', linkType)
Vue.component('dataSource', dataSource)
Vue.component('aceAdvert', aceAdvert)
Vue.component('floorTargeted', floorTargeted)
Vue.component('netAdvert', netAdvert)
Vue.component('ImageUpload', ImageUpload)
Vue.component('floorConfig', floorConfig)
Vue.config.devtools = true
/**
 * @description 注册admin内置插件
 */
installPlugin(Vue)
/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config
Vue.prototype.$axios = axios
Vue.prototype.$tools = tools
Vue.prototype.colorTransform = tools.colorTransform
store.commit('setAccess', ['/activeList', '/prizes', '/decorationManage', '/decorationList', '/healthApp', '/miniProgram'])

// axios.request({
//   url: '/jdh/menu/getAuthAppMenus',
//   method: 'get'
// }).then(res => {
//   if (res.data.code == 0) {
//     let auth = []
//     for (let i = 0; i < res.data.result.length; i++) {
//       auth.push(res.data.result[i].url)
//     }
//     store.commit('setAccess', auth)
//   } else {}
// }).catch(err => {
//   console.log(err)
// })

/*
  http://jdh.marketing.mobile.cms.jd.com //预发
  http://mobilecms.jd.com  //生产
 */

Vue.prototype.$publicUrl = config.baseUrl.pro; //'http://jdh.marketing.mobile.cms.jd.com';
Vue.prototype.$aceUrlDev = config.aceUrl.dev; //'http://jdh.marketing.mobile.cms.jd.com';
Vue.prototype.$aceUrl = config.aceUrl.pro; //'http://jdh.marketing.mobile.cms.jd.com';

/**
 * 注册指令
 */
importDirective(Vue)
Vue.directive('clickOutside', clickOutside)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})
